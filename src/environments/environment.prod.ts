export const environment = {
  production: false,
  SOCKET_ENDPOINT: 'http://localhost:3000',
  URL: 'http://192.168.0.16:5001/api',
  IP: '192.168.0.16',
};
