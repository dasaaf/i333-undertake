import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  //Public pages (Welcome pages)
  { path: 'registry', loadChildren: () => import('./modules/welcome-pages/registry-page/registry.module').then(m => m.RegistryModule) },
  { path: 'login', loadChildren: () => import('./modules/welcome-pages/login-page/login.module').then(m => m.LoginModule) },
  { path: 'contact', loadChildren: () => import('./modules/welcome-pages/contact-page/contact.module').then(m => m.ContactModule) },
  { path: 'password-recovery', loadChildren: () => import('./modules/welcome-pages/password-recovery/password-recovery.module').then(m => m.PasswordRecoveryModule) },
  { path: 'privacy-policies', loadChildren: () => import('./modules/welcome-pages/privacy-policies-page/privacy-policies.module').then(m => m.PrivacyPoliciesModule) },
  { path: 'terms', loadChildren: () => import('./modules/welcome-pages/terms-page/terms.module').then(m => m.TermsModule) },

  //Private pages (Home pages)
  { path: '', loadChildren: () => import('./modules/home-pages/home/home.module').then(m => m.HomeModule) },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

