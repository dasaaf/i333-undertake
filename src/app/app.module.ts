import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { registerLocaleData, DatePipe } from '@angular/common';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LoadingScreenInterceptor } from './interceptors/loading.interceptor';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { Config } from './config';
import localeCol from '@angular/common/locales/es-CO';
const socketConfig: SocketIoConfig = { url: Config.SOCKET_ENDPOINT, options: {} };
registerLocaleData(localeCol);


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(socketConfig)
  ],
  providers: [
    DatePipe,
    { provide: LOCALE_ID, useValue: 'es-CO' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
