import { ValidatorFn, FormControl, Validators } from '@angular/forms';

export class NumberValidator {

    static number(prms = {}): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } => {
            if (Validators.required(control)) {
                return null;
            }
            let val: number = control.value;
            if (isNaN(val) || /\D/.test(val.toString())) {
                return { 'notNumber': true };
            } else if (!isNaN(prms['min']) && !isNaN(prms['max'])) {
                return val < prms['min'] ? { "minLength": true } : val > prms['max'] ? { "maxLength": true } : null;
            } else if (!isNaN(prms['min'])) {
                return val < prms['min'] ? { "minLength": true } : null;
            } else if (!isNaN(prms['max'])) {
                return val > prms['max'] ? { "maxLength": true } : null;
            } else {
                return null;
            }
        };
    }
}