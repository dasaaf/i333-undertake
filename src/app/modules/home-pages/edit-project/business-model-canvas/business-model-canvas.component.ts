import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { BusinessModelCanvas, Project } from 'src/app/models/project';

@Component({
  selector: 'app-business-model-canvas',
  templateUrl: './business-model-canvas.component.html',
  styleUrls: ['./business-model-canvas.component.scss']
})
export class BusinessModelCanvasComponent implements OnInit, OnChanges {
  @Input() projectForm: FormGroup;
  @Input() project: Project;
  @Output() saveBMC = new EventEmitter<FormData>();
  BMCMatrixInvalid: boolean;
  formData: FormData;


  constructor(private projectFormBuilder: FormBuilder) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.projectForm.currentValue) {
      this.projectForm = changes.projectForm.currentValue;
    }
    if (changes.project.currentValue) {
      this.project = changes.project.currentValue;
      if (this.project.business_model_canvas) {
        this.chargeValuePropositions();
        this.chargeChannels();
        this.chargeCostStructure();
        this.chargeCustomerRelationships();
        this.chargeKeyActivities();
        this.chargeKeyResources();
        this.chargeKeyPartners();
        this.chargeRevenueStreams();
        this.chargeCustomerSegments();
      }
    }
  }

  save() {
    if (this.projectForm.valid) {
      this.BMCMatrixInvalid = false;
      this.formData = new FormData();
      let bmcMatrix: BusinessModelCanvas = new BusinessModelCanvas();
      bmcMatrix.channels = this.Channels.getRawValue();
      bmcMatrix.cost_structure = this.CostStructure.getRawValue();
      bmcMatrix.customer_relationships = this.CustomerRelationships.getRawValue();
      bmcMatrix.customer_segments = this.CustomerSegments.getRawValue();
      bmcMatrix.key_activities = this.KeyActivities.getRawValue();
      bmcMatrix.key_partners = this.KeyPartners.getRawValue();
      bmcMatrix.key_resources = this.KeyResources.getRawValue();
      bmcMatrix.revenue_streams = this.RevenueStreams.getRawValue();
      bmcMatrix.value_propositions = this.ValuePropositions.getRawValue();
      this.formData.append('business_model_canvas', JSON.stringify(bmcMatrix));
      this.saveBMC.emit(this.formData);
    } else {
      this.BMCMatrixInvalid = true;
    }
  }

  chargeValuePropositions() {
    if (this.project.business_model_canvas.value_propositions) {
      for (let value_proposition of this.project.business_model_canvas.value_propositions) {
        this.ValuePropositions.push(this.projectFormBuilder.group({
          description: new FormControl(value_proposition.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeKeyPartners() {
    if (this.project.business_model_canvas.key_partners) {
      for (let key_partner of this.project.business_model_canvas.key_partners) {
        this.KeyPartners.push(this.projectFormBuilder.group({
          description: new FormControl(key_partner.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeCustomerSegments() {
    if (this.project.business_model_canvas.customer_segments) {
      for (let customer_segment of this.project.business_model_canvas.customer_segments) {
        this.CustomerSegments.push(this.projectFormBuilder.group({
          description: new FormControl(customer_segment.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeKeyActivities() {
    if (this.project.business_model_canvas.key_activities) {
      for (let key_activity of this.project.business_model_canvas.key_activities) {
        this.KeyActivities.push(this.projectFormBuilder.group({
          description: new FormControl(key_activity.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeKeyResources() {
    if (this.project.business_model_canvas.key_resources) {
      for (let key_resource of this.project.business_model_canvas.key_resources) {
        this.KeyResources.push(this.projectFormBuilder.group({
          description: new FormControl(key_resource.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeCustomerRelationships() {
    if (this.project.business_model_canvas.customer_relationships) {
      for (let customer_relationship of this.project.business_model_canvas.customer_relationships) {
        this.CustomerRelationships.push(this.projectFormBuilder.group({
          description: new FormControl(customer_relationship.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeChannels() {
    if (this.project.business_model_canvas.channels) {
      for (let channel of this.project.business_model_canvas.channels) {
        this.Channels.push(this.projectFormBuilder.group({
          description: new FormControl(channel.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeCostStructure() {
    if (this.project.business_model_canvas.cost_structure) {
      for (let cost_structure of this.project.business_model_canvas.cost_structure) {
        this.CostStructure.push(this.projectFormBuilder.group({
          description: new FormControl(cost_structure.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  chargeRevenueStreams() {
    if (this.project.business_model_canvas.revenue_streams) {
      for (let revenue_stream of this.project.business_model_canvas.revenue_streams) {
        this.RevenueStreams.push(this.projectFormBuilder.group({
          description: new FormControl(revenue_stream.description, [Validators.maxLength(120)]),
        }));
      }
    }
  }

  get ValuePropositions() {
    return this.projectForm.get('value_propositions') as FormArray;
  }

  get KeyPartners() {
    return this.projectForm.get('key_partners') as FormArray;
  }

  get CustomerSegments() {
    return this.projectForm.get('customer_segments') as FormArray;
  }

  get KeyActivities() {
    return this.projectForm.get('key_activities') as FormArray;
  }

  get KeyResources() {
    return this.projectForm.get('key_resources') as FormArray;
  }

  get CustomerRelationships() {
    return this.projectForm.get('customer_relationships') as FormArray;
  }

  get Channels() {
    return this.projectForm.get('channels') as FormArray;
  }

  get CostStructure() {
    return this.projectForm.get('cost_structure') as FormArray;
  }

  get RevenueStreams() {
    return this.projectForm.get('revenue_streams') as FormArray;
  }

  addValuePropositions() {
    this.ValuePropositions.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addKeyPartner() {
    this.KeyPartners.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addCustomerSegments() {
    this.CustomerSegments.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addKeyActivity() {
    this.KeyActivities.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addKeyResource() {
    this.KeyResources.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addCustomerRelationships() {
    this.CustomerRelationships.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addChannel() {
    this.Channels.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addCostStructure() {
    this.CostStructure.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  addRevenueStream() {
    this.RevenueStreams.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120)]),
      })
    );
  }

  deleteKeyPartner(index) {
    this.KeyPartners.removeAt(index);
  }

  deleteValuePropositions(index) {
    this.ValuePropositions.removeAt(index);
  }

  deleteCustomerSegments(index) {
    this.CustomerSegments.removeAt(index);
  }

  deleteKeyActivity(index) {
    this.KeyActivities.removeAt(index);
  }

  deleteKeyResource(index) {
    this.KeyResources.removeAt(index);
  }

  deleteCustomerRelationships(index) {
    this.CustomerRelationships.removeAt(index);
  }

  deleteChannel(index) {
    this.Channels.removeAt(index);
  }

  deleteCostStructure(index) {
    this.CostStructure.removeAt(index);
  }

  deleteRevenueStream(index) {
    this.RevenueStreams.removeAt(index);
  }

}
