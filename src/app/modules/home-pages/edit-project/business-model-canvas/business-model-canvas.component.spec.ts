import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessModelCanvasComponent } from './business-model-canvas.component';

describe('BusinessModelCanvasComponent', () => {
  let component: BusinessModelCanvasComponent;
  let fixture: ComponentFixture<BusinessModelCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessModelCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessModelCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
