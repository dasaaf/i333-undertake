import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss'],
  providers: [ProjectService]
})
export class EditProjectComponent implements OnInit {
  project: Project;
  projectImageUrl: any;

  constructor(private route: ActivatedRoute, private router: Router, private projectService: ProjectService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    let id = this.route.snapshot.queryParamMap.get('id');
    if (id) {
      this.getProject(id);
      this.getProjectImage(id);
    }
  }

  getProjectImage(id: string) {
    this.projectService.getImage(id).subscribe(
      resp => {
        let url = URL.createObjectURL(resp);
        this.projectImageUrl = this.sanitizer.bypassSecurityTrustUrl(url);
      },
      error => {
        throw error;
      }
    );
  }

  getProject(id: string) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.projectService.getById(id).subscribe(
      resp => {
        if (resp['project'].user == currentUser._id) {
          this.project = resp['project'];
        } else {
          this.router.navigate(['projects']);
        }
      },
      error => {
        throw error;
      }
    );
  }
}
