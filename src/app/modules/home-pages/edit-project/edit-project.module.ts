import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditProjectRoutingModule } from './edit-project-routing.module';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { WostMatrixComponent } from './wost-matrix/wost-matrix.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { LocationsComponent } from './locations/locations.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusinessModelCanvasComponent } from './business-model-canvas/business-model-canvas.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';


@NgModule({
  declarations: [
    EditProjectComponent,
    BusinessModelCanvasComponent,
    WostMatrixComponent,
    ProjectFormComponent,
    LocationsComponent
  ],
  imports: [
    CommonModule,
    EditProjectRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MiniToolsModule
  ],
  exports: [
    ProjectFormComponent
  ],
  bootstrap: [
    EditProjectComponent
  ]
})
export class EditProjectModule { }
