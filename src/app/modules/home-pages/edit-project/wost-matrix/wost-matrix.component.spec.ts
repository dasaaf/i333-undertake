import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WostMatrixComponent } from './wost-matrix.component';

describe('WostMatrixComponent', () => {
  let component: WostMatrixComponent;
  let fixture: ComponentFixture<WostMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WostMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WostMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
