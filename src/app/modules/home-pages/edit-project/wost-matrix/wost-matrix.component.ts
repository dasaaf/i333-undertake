import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { SwotMatrix, Project } from 'src/app/models/project';

@Component({
  selector: 'app-wost-matrix',
  templateUrl: './wost-matrix.component.html',
  styleUrls: ['./wost-matrix.component.scss']
})
export class WostMatrixComponent implements OnInit, OnChanges {
  @Input() projectForm: FormGroup;
  @Input() project: Project;
  @Input() formData: FormData;
  @Output() saveWost = new EventEmitter<FormData>();
  WostMatrixInvalid: boolean;

  constructor(private projectFormBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.projectForm && changes.projectForm.currentValue) {
      this.projectForm = changes.projectForm.currentValue;
    }
    if (changes && changes.formData && changes.formData.currentValue) {
      this.formData = changes.formData.currentValue;
    }
    if (changes && changes.project && changes.project.currentValue) {
      this.project = changes.project.currentValue;
      if (this.project.swot_matrix) {
        this.chargeOpportunities();
        this.chargeStrengths();
        this.chargeThreats();
        this.chargeWeaknesses();
      }
    }
  }

  ngOnInit(): void {
  }

  save() {
    if (this.projectForm.valid) {
      this.WostMatrixInvalid = false;
      this.formData = new FormData();
      let swotMatrix: SwotMatrix = new SwotMatrix();
      swotMatrix.strengths = this.Strengths.getRawValue();
      swotMatrix.weaknesses = this.Weaknesses.getRawValue();
      swotMatrix.opportunities = this.Opportunities.getRawValue();
      swotMatrix.threats = this.Threats.getRawValue();
      this.formData.append('swot_matrix', JSON.stringify(swotMatrix));
      this.saveWost.emit(this.formData);
    } else {
      this.WostMatrixInvalid = true;
    }
  }

  chargeStrengths() {
    if (this.project.swot_matrix.strengths) {
      for (let strength of this.project.swot_matrix.strengths) {
        this.Strengths.push(this.projectFormBuilder.group({
          description: new FormControl(strength.description, [Validators.maxLength(120), Validators.required]),
        }));
      }
    }
  }

  chargeWeaknesses() {
    if (this.project.swot_matrix.weaknesses) {
      for (let weakness of this.project.swot_matrix.weaknesses) {
        this.Weaknesses.push(this.projectFormBuilder.group({
          description: new FormControl(weakness.description, [Validators.maxLength(120), Validators.required]),
        }));
      }
    }
  }

  chargeOpportunities() {
    if (this.project.swot_matrix.opportunities) {
      for (let opportunity of this.project.swot_matrix.opportunities) {
        this.Opportunities.push(this.projectFormBuilder.group({
          description: new FormControl(opportunity.description, [Validators.maxLength(120), Validators.required]),
        }));
      }
    }
  }

  chargeThreats() {
    if (this.project.swot_matrix.threats) {
      for (let threat of this.project.swot_matrix.threats) {
        this.Threats.push(this.projectFormBuilder.group({
          description: new FormControl(threat.description, [Validators.maxLength(120), Validators.required]),
        }));
      }
    }
  }

  get Strengths() {
    return this.projectForm.get('strengths') as FormArray;
  }

  get Weaknesses() {
    return this.projectForm.get('weaknesses') as FormArray;
  }

  get Opportunities() {
    return this.projectForm.get('opportunities') as FormArray;
  }

  get Threats() {
    return this.projectForm.get('threats') as FormArray;
  }

  addStrength() {
    this.Strengths.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120), Validators.required]),
      })
    );
  }

  addWeaknesses() {
    this.Weaknesses.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120), Validators.required]),
      })
    );
  }

  addOpportunities() {
    this.Opportunities.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120), Validators.required]),
      })
    );
  }

  addThreats() {
    this.Threats.push(
      this.projectFormBuilder.group({
        description: new FormControl("", [Validators.maxLength(120), Validators.required]),
      })
    );
  }

  deleteStrength(index) {
    this.Strengths.removeAt(index);
  }

  deleteWeaknesses(index) {
    this.Weaknesses.removeAt(index);
  }

  deleteOpportunities(index) {
    this.Opportunities.removeAt(index);
  }

  deleteThreats(index) {
    this.Threats.removeAt(index);
  }

}
