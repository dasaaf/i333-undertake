import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LocationService } from 'src/app/services/location.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss'],
  providers: [ProjectService, LocationService]
})
export class ProjectFormComponent implements OnInit, OnChanges {
  @Input() project: Project;
  @Output() pdfUrl = new EventEmitter<any>();
  loading: boolean;
  projectForm: FormGroup;
  currentUser: User;
  projectImageUrl: any;
  formInvalidMessage: boolean;
  formData: FormData;
  files: Array<any>;
  imageStyle: string;

  constructor(private projectService: ProjectService, private sanitizer: DomSanitizer, private projectFormBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.formInvalidMessage = false;
    this.files = new Array();
    this.formData = new FormData();
    this.createForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.project.currentValue && changes.project.currentValue._id) {
      this.project = changes.project.currentValue;
      this.createForm();
    }
  }

  getFileAttached(file_name: string, original_name: string) {
    this.projectService.getFileAttached(file_name).subscribe(
      resp => {
        this.pdfUrl.emit({ pdfUrl: this.arrayBufferToBase64(resp), pdfName: original_name });
      },
      error => {
        throw error;
      }
    );
  }

  showFileAttached(file: string, original_name) {
    this.getFileAttached(file, original_name);
  }

  arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      var fileName = event.target.files[0].name;
      reader.onload = (_event) => {
        this.compress(_event, fileName);
      }
    }
  }

  compress(_event, fileName) {
    let img: any = new Image();
    img.src = _event.target['result'];
    img.onload = () => {
      const elem = document.createElement('canvas');
      elem.width = 600;
      elem.height = elem.width * img.height / img.width;
      const ctx = elem.getContext('2d');
      ctx.drawImage(img, 0, 0, elem.width, elem.height);
      var canvasBlob = this.getCanvasBlob(ctx.canvas);
      canvasBlob.then((blob: Blob) => {
        let file = new File([blob], fileName, {
          type: 'image/jpeg',
          lastModified: Date.now()
        });
        let url = URL.createObjectURL(blob);
        this.projectImageUrl = this.sanitizer.bypassSecurityTrustUrl(url);
        this.projectForm.get('files').setValue(file);
      }, function (err) {
        console.log(err)
      });
    };
  }

  getCanvasBlob(canvas) {
    return new Promise(function (resolve, reject) {
      canvas.toBlob((blob) => {
        resolve(blob);
      }, 'image/jpeg', 1)
    })
  }

  initFormValues() {
    if (this.project == null || this.project._id == null) {
      this.project = new Project();
      this.projectImageUrl = null;
    } else {
      if (this.project.project_image) {
        this.projectService.getImage(this.project._id).subscribe(
          resp => {
            let objectURL = URL.createObjectURL(resp);
            this.projectImageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
          }, error => {
            throw new Error(error);
          }
        );
      }
    }
  }

  createForm() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.initFormValues();
    this.projectForm = this.projectFormBuilder.group({
      project_image: new FormControl(this.project.project_image != null ? this.project.project_image : ""),
      title: new FormControl(this.project.title, [Validators.required, Validators.maxLength(96)]),
      description: new FormControl(this.project.description, [Validators.required, Validators.maxLength(999)]),
      locations: this.projectFormBuilder.array([]),
      needs: new FormControl(this.project.needs, [Validators.required, Validators.maxLength(450)]),
      user: new FormControl(this.currentUser._id),
      state: new FormControl('ACTIVE'),
      files: [null],
      //SwotMatrix
      strengths: this.projectFormBuilder.array([]),
      weaknesses: this.projectFormBuilder.array([]),
      opportunities: this.projectFormBuilder.array([]),
      threats: this.projectFormBuilder.array([]),
      //BusinessModelCanvas
      key_partners: this.projectFormBuilder.array([]),
      value_propositions: this.projectFormBuilder.array([]),
      customer_segments: this.projectFormBuilder.array([]),
      key_activities: this.projectFormBuilder.array([]),
      key_resources: this.projectFormBuilder.array([]),
      customer_relationships: this.projectFormBuilder.array([]),
      channels: this.projectFormBuilder.array([]),
      cost_structure: this.projectFormBuilder.array([]),
      revenue_streams: this.projectFormBuilder.array([]),
    });
  }

  save(formData?: FormData) {
    if (formData) {
      this.formData = formData;
    }
    if (this.projectForm.valid) {
      if (this.project._id) {
        this.updateProject();
      } else {
        this.createProject();
      }
    } else {
      this.formInvalidMessage = true;
    }
  }

  receiveChanges(formData?: FormData) {
    if (formData) {
      this.formData = formData;
    }
  }

  updateProject() {
    for (var key in this.projectForm.value) {
      if (key != 'strengths' && key != 'weaknesses' && key != 'opportunities' && key != 'threats' && key != 'key_partners' && key != 'value_propositions' && key != 'customer_segments' && key != 'channels' && key != 'cost_structure' && key != 'revenue_streams' && key != 'key_activities' && key != 'key_resources' && key != 'customer_relationships' && key != 'locations') {
        this.formData.append(key, this.projectForm.value[key]);
      }
    }
    this.loading = true;
    this.projectService.update(this.formData, this.project._id, (resp, error) => {
      if (resp) {
        this.loading = false;
        this.formData = new FormData();
      }
      if (error) {
        throw new Error(error);
      }
    });
  }

  createProject() {
    for (var key in this.projectForm.value) {
      if (key != 'strengths' && key != 'weaknesses' && key != 'opportunities' && key != 'threats' && key != 'key_partners' && key != 'value_propositions' && key != 'customer_segments' && key != 'channels' && key != 'cost_structure' && key != 'revenue_streams' && key != 'key_activities' && key != 'key_resources' && key != 'customer_relationships' && key != 'locations') {
        this.formData.append(key, this.projectForm.value[key]);
      }
    }
    this.projectService.create(this.formData,
      (resp, error) => {
        if (resp) {
          this.formData = new FormData();
          this.router.navigate(['/projects']);
        }
        if (error) {
          console.log(error);
        }
      });
  }

  setStyleImage(dimension: any) {
    if (dimension.width >= dimension.height) {
      this.imageStyle = 'image-preview horizontal-format';
    } else {
      this.imageStyle = 'image-preview vertical-format';
    }
  }
}
