import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit, OnChanges {
  @Input() projectForm: FormGroup;
  @Input() project: Project;
  @Output() saveLocation = new EventEmitter<FormData>();
  formData: FormData;
  countries: JSON;
  cities: Array<JSON>;
  states: Array<JSON>;


  constructor(private locationService: LocationService, private projectFormBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.cities = new Array<JSON>();
    this.states = new Array<JSON>();
    this.getCountries();
    this.chargeLocations();
  }

  ngOnInit(): void {
  }

  getCountries() {
    this.locationService.getCountries().subscribe(
      response => {
        this.countries = response;
      },
      error => {
        throw error;
      }
    );
  }

  getStates(countryId: string, location?: any) {
    this.locationService.getStates(countryId).subscribe(
      response => {
        this.states.push(response);
        if (location && location.state) {
          this.getCities(location.state, location);
        }
      },
      error => {
        throw error;
      }
    );
  }

  getCities(stateId: string, location?: any) {
    this.locationService.getCities(stateId).subscribe(
      response => {
        this.cities.push(response);
        if (location) {
          this.Locations.push(this.projectFormBuilder.group({
            country: new FormControl(location.country),
            state: new FormControl(location.state),
            city: new FormControl(location.city)
          }));
          this.sendChanges();
        }
      },
      error => {
        throw error;
      }
    );
  }

  chargeLocations() {
    if (this.project && this.project.locations) {
      for (let location of this.project.locations) {
        if (location.country) {
          this.getStates(location.country, location);
        }
      }
    }
  }

  get Locations() {
    return this.projectForm.get('locations') as FormArray;
  }

  addLocation() {
    this.Locations.push(
      this.projectFormBuilder.group({
        country: new FormControl(""),
        state: new FormControl(""),
        city: new FormControl("")
      })
    );
  }

  deleteLocation(index) {
    this.Locations.removeAt(index);
    this.states.splice(index, 1);
    this.cities.splice(index, 1);
  }

  sendChanges() {
    if (this.projectForm.valid) {
      this.formData = new FormData();
      this.Locations.getRawValue().forEach(e => {
        this.formData.append('locations', JSON.stringify(e));
      });
    }
    this.saveLocation.emit(this.formData);
  }

}
