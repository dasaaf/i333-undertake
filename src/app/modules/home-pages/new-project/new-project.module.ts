import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewProjectRoutingModule } from './new-project-routing.module';
import { NewProjectComponent } from './new-project/new-project.component';
import { EditProjectModule } from '../edit-project/edit-project.module';


@NgModule({
  declarations: [
    NewProjectComponent
  ],
  imports: [
    CommonModule,
    NewProjectRoutingModule,
    EditProjectModule
  ],
  bootstrap: [NewProjectComponent]
})
export class NewProjectModule { }
