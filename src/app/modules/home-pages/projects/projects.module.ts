import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { NewsModule } from '../news/news.module';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';
import { CardsModule } from '../../components/cards/cards.module';


@NgModule({
  declarations: [
    ProjectsListComponent
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    NewsModule,
    MiniToolsModule,
    CardsModule
  ],
  bootstrap: [ProjectsListComponent]
})
export class ProjectsModule { }
