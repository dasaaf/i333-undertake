import { Component, OnInit } from '@angular/core';
import { User } from '../../../../models/user';
import { ProjectService } from '../../../../services/project.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss'],
  providers: [ProjectService]
})
export class ProjectsListComponent implements OnInit {
  page: any;
  projectsList: any;
  currentUser: User;
  configCardsCollection: any;
  recordsPerPage: number;
  deleteMessage: any;
  projectToDelete: any;

  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.recordsPerPage = 9;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (!params['page']) {
        this.page = 1;
      } else {
        this.page = params['page'];
      }
      this.getProjects();
    });
  }

  getProjects() {
    let userId = this.currentUser._id;
    this.projectService.getAll(userId, this.recordsPerPage, this.page).subscribe(
      response => {
        this.configCardsCollection = {
          'configCard': {
            'actions': true,
            'deleteAction': false,
            'shareAction': false,
            'doAQuestionAction': false,
            'actionsHoverAnimation': true,
            'descriptionHoverAnimation': true,
            'pagesNumber': 0
          }
        };
        this.configCardsCollection.configCard.pagesNumber = response['pages_number'];
        this.projectsList = response['project'];
        this.getProjectsImage();
      },
      error => {
        throw error;
      }
    );
  }

  getProjectsImage() {
    var aux: any = new Array(this.projectsList.length);
    for (let i = 0; i < this.projectsList.length; i++) {
      let project = this.projectsList[i];
      this.projectService.getImage(this.projectsList[i]._id).subscribe(
        response => {
          project.image = response;
          aux[i] = project;
        },
        error => {
          console.log(error);
        }
      )
    }
    this.projectsList = aux;
  }

  showProjectPage(project: any) {
    this.router.navigate(['/project-dashboard'], { queryParams: { id: project.item_selected._id } });
  }

  goToPage(pageSelected: number) {
    this.router.navigate(['/projects'], { queryParams: { page: pageSelected } });
  }

  deleteProject(project: any) {
    this.deleteMessage = true;
    this.projectToDelete = project.item_selected;
  }

  confirmDeleteProject() {
    this.deleteMessage = null;
    this.projectService.delete(this.projectToDelete._id).subscribe(
      resp => {
        this.router.navigate(['/projects'], { queryParams: { page: 1 } });
      },
      error => {
        throw error;
      }
    )
  }

  closeMessage() {
    this.deleteMessage = null;
  }

}
