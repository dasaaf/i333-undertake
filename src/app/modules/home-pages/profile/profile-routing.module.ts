import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileEditComponent } from './profile-edit/profile-edit/profile-edit.component';
import { ProfilePublicComponent } from './profile-public/profile-public.component';

const routes: Routes = [
  { path: 'user', component: ProfileEditComponent },
  { path: 'expert', component: ProfilePublicComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
