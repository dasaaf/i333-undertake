import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Rating } from 'src/app/models/rating';
import { RatingService } from 'src/app/services/rating.service';

@Component({
  selector: 'app-profile-public',
  templateUrl: './profile-public.component.html',
  styleUrls: ['./profile-public.component.scss'],
  providers: [UserService, RatingService]
})
export class ProfilePublicComponent implements OnInit {
  user: User;
  currentUser: User;
  imgURL: any;
  rating: Rating;
  imageStyle: string;
  score: any;
  commentsList: any;


  constructor(private userService: UserService, private sanitizer: DomSanitizer, private route: ActivatedRoute, private ratingService: RatingService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let id = this.route.snapshot.queryParamMap.get('id');
    if (id) {
      this.getUser(id);
    }
  }

  getUser(id) {
    this.userService.getById(id).subscribe(
      resp => {
        this.user = resp['user'];
        this.getRating();
        this.getScore();
        this.getPhoto();
      },
      error => {
        throw error;
      }
    )
  }

  getPhoto() {
    this.userService.getPhoto(this.user._id).subscribe(
      response => {
        let objectURL = URL.createObjectURL(response);
        this.imgURL = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      error => {
        throw error;
      }
    );
  }

  getRating() {
    this.ratingService.getRating(this.user._id, this.currentUser._id).subscribe(
      response => {
        if (!response || response['rating'].length == 0) {
          this.rating = new Rating();
        } else {
          this.rating = response['rating'][0];
        }
      },
      error => {
        throw error;
      }
    );
  }


  getScore() {
    this.ratingService.getScore(this.user._id).subscribe(
      response => {
        if (response['rating'].length > 0) {
          this.score = Math.round((response['rating'][0].score + Number.EPSILON) * 100) / 100;
        }
      },
      error => {
        throw error;
      }
    );
  }

  qualify(qualification: number) {
    if (!this.rating._id) {
      this.rating.expert = this.user._id;
      this.rating.grader = this.currentUser._id;
      this.rating.qualification = qualification;
      this.ratingService.qualify(this.rating).subscribe(
        response => {
          this.rating = response['rating'];
          this.getScore();
        },
        error => {
          throw error;
        }
      );
    } else {
      this.rating.qualification = qualification;
      this.ratingService.updateQualify(this.rating).subscribe(
        response => {
          this.rating = response['rating'];
          this.getScore();
        },
        error => {
          throw error;
        }
      );
    }
  }

  setStyleImage(dimension: any) {
    if (dimension.width >= dimension.height) {
      this.imageStyle = 'horizontal-format';
    } else {
      this.imageStyle = 'vertical-format';
    }
  }

}
