import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePublicComponent } from './profile-public/profile-public.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';
import { ProfileEditComponent } from './profile-edit/profile-edit/profile-edit.component';
import { ProfileHeaderComponent } from './profile-edit/profile-header/profile-header.component';
import { BeAnExpertComponent } from './profile-edit/be-an-expert/be-an-expert.component';
import { PersonalInformationComponent } from './profile-edit/personal-information/personal-information.component';
import { AcademicTrainingComponent } from './profile-edit/academic-training/academic-training.component';
import { SkillsComponent } from './profile-edit/skills/skills.component';
import { LaboralExperiencesComponent } from './profile-edit/laboral-experiences/laboral-experiences.component';
import { ReferencesComponent } from './profile-edit/references/references.component';
import { DocumentsComponent } from './profile-edit/documents/documents.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProfileEditComponent,
    ProfilePublicComponent,
    ProfileHeaderComponent,
    BeAnExpertComponent,
    PersonalInformationComponent,
    AcademicTrainingComponent,
    SkillsComponent,
    LaboralExperiencesComponent,
    ReferencesComponent,
    DocumentsComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MiniToolsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [
    ProfileEditComponent,
    ProfilePublicComponent
  ]
})
export class ProfileModule { }
