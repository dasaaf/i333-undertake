import { Component, Input, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-be-an-expert',
  templateUrl: './be-an-expert.component.html',
  styleUrls: ['./be-an-expert.component.scss'],
  providers: [UserService]
})
export class BeAnExpertComponent implements OnInit {
  @Input() currentUser;
  requestRoleMessage: string;
  
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  sendCurriculum() {
    this.userService.sendCurriculum(this.currentUser._id).subscribe(
      resp => {
        this.requestRoleMessage = "Gracias por enviarnos tu solicitud, estaremos dando respuesta en el menor tiempo posible.";
      },
      error => {
        this.requestRoleMessage = "No se ha podido enviar la solicitud, probablemente ya exista una solicitud en proceso";
        throw error;
      }
    );
  }

}
