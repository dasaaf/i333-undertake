import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-academic-training',
  templateUrl: './academic-training.component.html',
  styleUrls: ['./academic-training.component.scss']
})
export class AcademicTrainingComponent implements OnInit, OnChanges {
  @Input() currentUser: User;
  @Input() profileForm: FormGroup;
  @Output() formChanges = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.profileForm = changes.profileForm.currentValue;
    this.currentUser = changes.currentUser.currentValue;
    this.chargeAcademicTraining();
  }

  ngOnInit(): void {
    this.profileForm.get("academic_training").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    });
  }

  chargeAcademicTraining() {
    if (this.currentUser.academic_training && this.currentUser.academic_training.length > 0) {
      for (let item of this.currentUser.academic_training) {
        this.academicTraining.push(this.formBuilder.group({
          academic_title: new FormControl(item.academic_title, [Validators.required, Validators.maxLength(60)]),
          institution: new FormControl(item.institution, [Validators.required, Validators.maxLength(60)]),
          state: new FormControl(item.state, [Validators.required]),
          start_date: new FormControl(item.start_date, [Validators.required]),
          end_date: new FormControl(item.end_date)
        }));
      }
    }
  }

  addAcademicTraining() {
    this.academicTraining.push(
      this.formBuilder.group({
        academic_title: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        institution: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        state: new FormControl("", [Validators.required]),
        start_date: new FormControl(null, [Validators.required]),
        end_date: new FormControl(null)
      })
    );
  }

  deleteAcademicTraining(index) {
    this.academicTraining.removeAt(index);
  }

  get academicTraining() {
    return this.profileForm.get('academic_training') as FormArray;
  }

}
