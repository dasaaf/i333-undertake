import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.scss']
})
export class ReferencesComponent implements OnInit, OnChanges {
  @Input() currentUser: User;
  @Input() profileForm: FormGroup;
  @Output() formChanges = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.profileForm = changes.profileForm.currentValue;
    this.currentUser = changes.currentUser.currentValue;
    this.chargeReferences();
  }

  ngOnInit(): void {
    this.profileForm.get("references").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    });
  }

  chargeReferences() {
    if (this.currentUser.references && this.currentUser.references.length > 0) {
      for (let laboralReference of this.currentUser.references) {
        this.references.push(this.formBuilder.group({
          first_name: new FormControl(laboralReference.first_name, [Validators.required, Validators.maxLength(60)]),
          last_name: new FormControl(laboralReference.last_name, [Validators.required, Validators.maxLength(60)]),
          phone1: new FormControl(laboralReference.phone1, [Validators.required, Validators.maxLength(30)]),
          phone2: new FormControl(laboralReference.phone2, [Validators.maxLength(30)]),
          position: new FormControl(laboralReference.position, [Validators.maxLength(60)]),
          title: new FormControl(laboralReference.title, [Validators.maxLength(60)]),
        }));
      }
    }
  }

  addReference() {
    this.references.push(
      this.formBuilder.group({
        first_name: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        last_name: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        title: new FormControl("", [Validators.maxLength(120)]),
        position: new FormControl("", [Validators.maxLength(120)]),
        phone1: new FormControl("", [Validators.required, Validators.maxLength(30)]),
        phone2: new FormControl("", [Validators.maxLength(30)])
      })
    );
  }

  deleteReference(index) {
    this.references.removeAt(index);
  }

  get references() {
    return this.profileForm.get('references') as FormArray;
  }

}
