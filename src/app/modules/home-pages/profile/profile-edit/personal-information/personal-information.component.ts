import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss'],
})
export class PersonalInformationComponent implements OnInit, OnChanges {
  @Input() profileForm: FormGroup;
  @Output() formChanges = new EventEmitter<FormGroup>();
  birthDate: any;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.profileForm = changes.profileForm.currentValue;
  }

  ngOnInit(): void {
    this.profileForm.get("first_name").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    });
    this.profileForm.get("last_name").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    })
    this.profileForm.get("birth_date").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    })
    this.profileForm.get("title").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    })
    this.profileForm.get("professional_profile").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    })
  }

}
