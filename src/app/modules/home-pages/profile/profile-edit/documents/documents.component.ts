import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import { Config } from 'src/app/config';
import { User } from 'src/app/models/user';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  providers: [UserService, StorageSessionService]
})
export class DocumentsComponent implements OnInit, OnChanges {
  @Input() profileForm: FormGroup;
  @Input() currentUser: User;
  @Output() files = new EventEmitter<Array<any>>();
  filesAttached: Array<any>;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private storageSessionService: StorageSessionService) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.currentUser = changes.currentUser.currentValue;
    this.profileForm = changes.profileForm.currentValue;
  }

  ngOnInit(): void {
    this.filesAttached = [];
    this.profileForm.get("certificates").valueChanges.subscribe(x => {
      this.files.emit(this.filesAttached);
    });
  }

  onFileChange(event: any) {
    const files = event.target.files;
    if (files.length > 0) {
      if (files[0].type === 'application/pdf') {
        this.filesAttached.push(files[0]);
        this.profileForm.get('filesAttached').setValue(this.filesAttached);
      } else {
        alert('El archivo seleccionado no está permitido entre los formatos, por favor seleccione un archivo en formato PDF');
      }
    }
  }

  removeCertificate(certificate: any) {
    this.userService.removeCertificate(certificate).subscribe(
      response => {
        this.userService.getById(this.currentUser._id).subscribe(
          resp => {
            const user: any = resp['user'];
            user.password = '';
            this.storageSessionService.setItem('currentUser', JSON.stringify(user));
          },
          error => {
            throw new Error(error);
          }
        )
      }, error => {
        throw new Error(error);
      }
    );
  }

  downloadCertificate(certificate: any) {
    this.userService.downloadCertificate(certificate).subscribe(
      response => {
        console.log(response);
        const objectUrl = URL.createObjectURL(new Blob([response], { type: Config.MIME_TYPES.pdf }));
        saveAs(objectUrl, certificate.name + '.pdf');
      }, error => {
        throw new Error(error);
      }
    );
  }

  addCertificate() {
    this.certificates.push(
      this.formBuilder.group({
        document: new FormControl("", [Validators.required]),
      })
    );
  }

  deleteCertificate(index) {
    this.certificates.removeAt(index);
  }

  get certificates() {
    return this.profileForm.get('certificates') as FormArray;
  }
}
