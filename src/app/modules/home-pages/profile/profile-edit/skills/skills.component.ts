import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { NumberValidator } from 'src/app/validations/number-validator';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})

export class SkillsComponent implements OnInit, OnChanges {
  @Input() currentUser: User;
  @Input() profileForm: FormGroup;
  @Output() formChanges = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.currentUser = changes.currentUser.currentValue;
    this.profileForm = changes.profileForm.currentValue;
    this.chargeSkills();
  }

  ngOnInit(): void {
    this.profileForm.get("skills").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    });
  }

  chargeSkills() {
    if (this.currentUser.skills && this.currentUser.skills.length > 0) {
      for (let item of this.currentUser.skills) {
        this.skills.push(this.formBuilder.group({
          name: new FormControl(item.name, [Validators.required, Validators.maxLength(60)]),
          level: new FormControl(item.level, [Validators.required, NumberValidator.number({ min: 0 }), NumberValidator.number({ max: 10 })]),
        }));
      }
    }
  }

  addSkill() {
    this.skills.push(
      this.formBuilder.group({
        name: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        level: new FormControl(1, [Validators.required, NumberValidator.number({ min: 0 }), NumberValidator.number({ max: 10 })]),
      })
    );
  }

  deleteSkill(index) {
    this.skills.removeAt(index);
  }


  get skills() {
    return this.profileForm.get('skills') as FormArray;
  }

}
