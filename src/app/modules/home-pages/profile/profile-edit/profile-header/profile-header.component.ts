import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { User } from 'src/app/models/user';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { UserService } from 'src/app/services/user.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss'],
  providers: [UserService]
})
export class ProfileHeaderComponent implements OnInit, OnChanges {
  @Input() currentUser: User;
  @Output() onPhotoSelected = new EventEmitter<any>();
  userImageUrl: any;
  imageValidation: string;
  imageStyle: string;
  changePass: boolean = false;

  constructor(private userService: UserService, private storageSessionService: StorageSessionService, private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.currentUser = changes.currentUser.currentValue;
  }

  ngOnInit(): void {
    this.getPhoto();
    this.storageSessionService.watchStorage().subscribe((data: string) => {
      this.getPhoto();
    });
  }

  onPhotoChange(event: any) {
    if (event.target.files.length > 0) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      var fileName = event.target.files[0].name;
      var type = event.target.files[0].type;
      if (type.split('/')[1] == 'jpeg' || type.split('/')[1] == 'png') {
        reader.onload = (_event) => {
          this.compress(_event, fileName);
        }
      } else {
        this.imageValidation = "Sólo está permitido subir imágenes .jpg o .png";
      }
    }
  }

  getPhoto() {
    this.userService.getPhoto(this.currentUser._id).subscribe(
      response => {
        let objectURL = URL.createObjectURL(response);
        this.userImageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      error => {
        throw error;
      }
    );
  }

  compress(_event, fileName) {
    let img: any = new Image();
    img.src = _event.target['result'];
    img.onload = () => {
      const elem = document.createElement('canvas');
      elem.width = 600;
      elem.height = elem.width * img.height / img.width;
      const ctx = elem.getContext('2d');
      ctx.drawImage(img, 0, 0, elem.width, elem.height);
      var canvasBlob = this.getCanvasBlob(ctx.canvas);
      canvasBlob.then((blob: Blob) => {
        let file = new File([blob], fileName, {
          type: 'image/jpeg',
          lastModified: Date.now()
        });
        let url = URL.createObjectURL(blob);
        this.userImageUrl = this.sanitizer.bypassSecurityTrustUrl(url);
        this.onPhotoSelected.emit(file);
      }, function (error) {
        throw error;
      });
    };
  }

  getCanvasBlob(canvas) {
    return new Promise(function (resolve, reject) {
      canvas.toBlob((blob) => {
        resolve(blob);
      }, 'image/jpeg', 1)
    })
  }

  setStyleImage(dimension: any) {
    if (dimension.width >= dimension.height) {
      this.imageStyle = 'image-preview horizontal-format';
    } else {
      this.imageStyle = 'image-preview vertical-format';
    }
  }

  showOptions(){
    this.changePass = !this.changePass;
  }
  
}
