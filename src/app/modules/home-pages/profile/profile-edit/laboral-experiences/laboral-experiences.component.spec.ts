import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaboralExperiencesComponent } from './laboral-experiences.component';

describe('LaboralExperiencesComponent', () => {
  let component: LaboralExperiencesComponent;
  let fixture: ComponentFixture<LaboralExperiencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaboralExperiencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaboralExperiencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
