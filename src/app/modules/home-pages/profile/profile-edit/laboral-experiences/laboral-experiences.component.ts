import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-laboral-experiences',
  templateUrl: './laboral-experiences.component.html',
  styleUrls: ['./laboral-experiences.component.scss']
})
export class LaboralExperiencesComponent implements OnInit, OnChanges {
  @Input() currentUser: User;
  @Input() profileForm: FormGroup;
  @Output() formChanges = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.profileForm = changes.profileForm.currentValue;
    this.currentUser = changes.currentUser.currentValue;
    this.chargeLaboralExperiences();
  }

  ngOnInit(): void {
    this.profileForm.get("laboral_experiences").valueChanges.subscribe(x => {
      this.formChanges.emit(this.profileForm);
    });
  }

  chargeLaboralExperiences() {
    if (this.currentUser.laboral_experiences && this.currentUser.laboral_experiences.length > 0) {
      for (let item of this.currentUser.laboral_experiences) {
        this.laboralExperiences.push(this.formBuilder.group({
          institution: new FormControl(item.institution, [Validators.maxLength(60)]),
          position: new FormControl(item.position, [Validators.maxLength(60)]),
          description: new FormControl(item.description, [Validators.maxLength(255)]),
          start_date: new FormControl(item.start_date, [Validators.required]),
          end_date: new FormControl(item.end_date)
        }));
      }
    }
  }

  addLaboralExperience() {
    this.laboralExperiences.push(
      this.formBuilder.group({
        institution: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        position: new FormControl("", [Validators.required, Validators.maxLength(60)]),
        description: new FormControl("", [Validators.maxLength(255)]),
        start_date: new FormControl(null, [Validators.required]),
        end_date: new FormControl(null)
      })
    );
  }

  deleteLaboralExperience(index) {
    this.laboralExperiences.removeAt(index);
  }

  get laboralExperiences() {
    return this.profileForm.get('laboral_experiences') as FormArray;
  }

}
