import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss'],
  providers: [UserService, StorageSessionService]
})
export class ProfileEditComponent implements OnInit {
  currentUser: User;
  profileForm: FormGroup;
  birthDate: any;
  accountConfig: boolean;
  showPassChangeMessage: boolean;
  messageInvalid: boolean;
  requestRoleMessage: string;
  loading: boolean;
  filesAttached: Array<any> = new Array();
  beAnExpert: boolean = false;

  constructor(private userService: UserService, private storageSessionService: StorageSessionService, private formBuilder: FormBuilder, private datePipe: DatePipe) {
    this.accountConfig = false;
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.detectSessionChanges();
    this.createUserForm();
  }

  detectSessionChanges() {
    this.storageSessionService.watchStorage().subscribe((data: string) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    })
  }

  setPhoto(file: any) {
    this.profileForm.get('photo_file').setValue(file);
  }

  formChange(profileForm: FormGroup) {
    this.profileForm = profileForm;
  }

  filesChange(filesAttached: Array<any>) {
    this.filesAttached = filesAttached;
  }

  createUserForm() {
    this.birthDate = this.datePipe.transform(this.currentUser.birth_date, 'yyyy-MM-dd');
    this.profileForm = this.formBuilder.group({
      photo: new FormControl(this.currentUser.photo == null ? this.currentUser.photo : ""),
      first_name: new FormControl(this.currentUser.first_name != 'UNDEFINED' ? this.currentUser.first_name : "", [Validators.required]),
      last_name: new FormControl(this.currentUser.last_name != 'UNDEFINED' ? this.currentUser.last_name : "", [Validators.required]),
      birth_date: new FormControl(this.birthDate != 'UNDEFINED' ? this.birthDate : null),
      title: new FormControl(this.currentUser.title != 'UNDEFINED' ? this.currentUser.title : ""),
      professional_profile: new FormControl(this.currentUser.professional_profile != 'UNDEFINED' ? this.currentUser.professional_profile : "", [Validators.maxLength(600)]),
      academic_training: this.formBuilder.array([]),
      skills: this.formBuilder.array([]),
      laboral_experiences: this.formBuilder.array([]),
      references: this.formBuilder.array([]),
      personal_references: this.formBuilder.array([]),
      certificates: this.formBuilder.array([]),
      filesAttached: [],
      photo_file: [null],
    });
  }

  save() {
    this.messageInvalid = false;
    if (this.profileForm.valid) {
      let formData = new FormData();
      let birth_date = new Date(this.datePipe.transform(this.profileForm.get('birth_date').value, 'yyyy/MM/dd'));
      birth_date.setDate(birth_date.getDate());
      this.profileForm.get('birth_date').setValue(birth_date);
      for (var key in this.profileForm.value) {
        if (this.profileForm.get('photo').value != null && this.profileForm.get('photo').value != '') {
          formData.append(key, this.profileForm.value[key]);
        } else if (key != 'photo' && key != 'skills' && key != 'academic_training' && key != 'laboral_experiences' && key != 'references' && key != 'certificates') {
          formData.append(key, this.profileForm.value[key]);
        }
        if (key === 'skills') {
          this.skills.getRawValue().forEach(e => {
            formData.append('skills', JSON.stringify(e));
          });
        }
        if (key === 'academic_training') {
          this.academicTraining.getRawValue().forEach(e => {
            formData.append('academic_training', JSON.stringify(e));
          });
        }
        if (key === 'laboral_experiences') {
          this.laboralExperiences.getRawValue().forEach(e => {
            formData.append('laboral_experiences', JSON.stringify(e));
          });
        }
        if (key === 'references') {
          this.references.getRawValue().forEach(e => {
            formData.append('references', JSON.stringify(e));
          });
        }
        if (key === 'filesAttached') {
          this.filesAttached.forEach(document => {
            formData.append('certificates', document);
          });
        }

      }
      this.update(formData);
    }
    else {
      this.messageInvalid = true;
    }
  }

  update(formData) {
    this.loading = true;
    this.userService.update(formData, this.currentUser._id,
      (resp, error) => {
        this.loading = false;
        if (resp) {
          this.userService.getById(this.currentUser._id).subscribe(
            response => {
              let user: any = response['user'];
              user.password = '';
              this.storageSessionService.setItem('currentUser', JSON.stringify(user));
            },
            error => {
              throw new Error(error);
            }
          )
        }
        if (error) {
          throw new Error("It is don't can update the profile");
        }
      });
  }

  showBeAnExpert() {
    this.beAnExpert = !this.beAnExpert;
  }

  showPassChange(success: boolean) {
    this.showPassChangeMessage = success;
  }

  dropdownAccount() {
    this.accountConfig = !this.accountConfig;
  }

  get academicTraining() {
    return this.profileForm.get('academic_training') as FormArray;
  }

  get skills() {
    return this.profileForm.get('skills') as FormArray;
  }

  get laboralExperiences() {
    return this.profileForm.get('laboral_experiences') as FormArray;
  }

  get references() {
    return this.profileForm.get('references') as FormArray;
  }
}
