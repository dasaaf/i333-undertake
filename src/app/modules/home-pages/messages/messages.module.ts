import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesDashboardComponent } from './messages-dashboard/messages-dashboard.component';
import { ConvertationsListComponent } from './convertations-list/convertations-list.component';
import { SendMessageComponent } from './send-message/send-message.component';
import { MessagesListComponent } from './messages-list/messages-list.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';
import { SendPriceProposalComponent } from './send-price-proposal/send-price-proposal.component';


@NgModule({
  declarations: [
    MessagesDashboardComponent,
    ConvertationsListComponent,
    SendMessageComponent,
    MessagesListComponent,
    SendPriceProposalComponent
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    MiniToolsModule
  ],
  bootstrap: [MessagesDashboardComponent]
})
export class MessagesModule { }
