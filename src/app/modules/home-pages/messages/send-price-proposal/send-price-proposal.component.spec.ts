import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendPriceProposalComponent } from './send-price-proposal.component';

describe('SendPriceProposalComponent', () => {
  let component: SendPriceProposalComponent;
  let fixture: ComponentFixture<SendPriceProposalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendPriceProposalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendPriceProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
