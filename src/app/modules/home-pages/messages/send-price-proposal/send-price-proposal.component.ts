import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-send-price-proposal',
  templateUrl: './send-price-proposal.component.html',
  styleUrls: ['./send-price-proposal.component.scss']
})
export class SendPriceProposalComponent implements OnInit {
  @Output() visible = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  closeMessage() {
    this.visible.emit(false);
  }

}
