import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagesDashboardComponent } from './messages-dashboard/messages-dashboard.component';

const routes: Routes = [
  { path: '', component: MessagesDashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
