import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { User } from 'src/app/models/user';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.scss'],
  providers: [UserService]
})
export class MessagesListComponent implements OnInit, OnChanges {
  @Input() userSelected: User;
  @Input() messageList: any;
  @ViewChild('messagesListContainer') private messagesListContainer: ElementRef;
  imageStyle: string;
  currentUser: any;


  constructor(private sanitizer: DomSanitizer, private userService: UserService) { }

  ngOnChanges(changes: SimpleChanges): void {    
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (changes && changes.userSelected && changes.userSelected.currentValue) {
      this.getPhoto();
    }
    if (changes && changes.messageList && changes.messageList.currentValue) {
      this.messageList = changes.messageList.currentValue;
    }
  }

  ngOnInit(): void {
    this.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  setStyleImage(dimension: any) {
    if (dimension.width >= dimension.height) {
      this.imageStyle = 'horizontal-format';
    } else {
      this.imageStyle = 'vertical-format';
    }
  }

  getPhoto() {
    if (this.userSelected._id) {
      this.userService.getPhoto(this.userSelected._id).subscribe(
        response => {
          let objectURL = URL.createObjectURL(response);
          this.userSelected["image"] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        },
        error => {
          throw error;
        }
      );
    }
  }

  scrollToBottom(): void {
    try {
      this.messagesListContainer.nativeElement.scrollTop = this.messagesListContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }
}
