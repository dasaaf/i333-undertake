import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { User } from 'src/app/models/user';
import { Message } from 'src/app/models/message';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss'],
  providers: [MessageService]
})
export class SendMessageComponent implements OnInit, OnChanges {
  @Input() userSelected;
  @Output() messageSend = new EventEmitter<boolean>();
  proposalVisible: boolean = false;
  currentUser: User;

  constructor(private messageService: MessageService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.userSelected && changes.userSelected.currentValue) {
      this.userSelected = changes.userSelected.currentValue;
    }
  }

  ngOnInit(): void {

  }

  sendMessage(textMessage: string) {
    let message: Message = new Message();
    message.content = textMessage["innerText"];
    message.user_sender = this.currentUser._id;
    message.user_receiver = this.userSelected._id;
    this.messageService.createMessage(message).subscribe(
      resp => {
        textMessage["innerText"] = "";
        this.messageSend.emit(true);
      },
      error => {
        console.log(error);
      }
    );
  }

  seeProposalForm(visible: boolean) {
    this.proposalVisible = visible;
  }
}
