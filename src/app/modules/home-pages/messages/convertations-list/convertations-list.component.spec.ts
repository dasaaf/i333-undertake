import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertationsListComponent } from './convertations-list.component';

describe('ConvertationsListComponent', () => {
  let component: ConvertationsListComponent;
  let fixture: ComponentFixture<ConvertationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
