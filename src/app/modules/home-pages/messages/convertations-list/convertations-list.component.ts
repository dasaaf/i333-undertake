import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-convertations-list',
  templateUrl: './convertations-list.component.html',
  styleUrls: ['./convertations-list.component.scss'],
  providers: [MessageService, UserService],
})
export class ConvertationsListComponent implements OnInit, OnChanges {
  @Output() userSelected = new EventEmitter<User>();
  @Input() changeUserSelected: any;
  imageStyle: string;
  convertationsList: Array<any>;
  convertationsListFull: Array<any>;
  currentUser: User;
  loading: boolean;
  userIds: any;

  constructor(private messageService: MessageService, private userService: UserService, private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.changeUserSelected && changes.changeUserSelected.currentValue) {
      this.changeUserSelected = changes.changeUserSelected.currentValue;
    }
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.getConvertationsList();
  }

  setStyleImage(dimension: any, tag: any) {
    if (dimension) {
      if (dimension.width > dimension.height) {
        tag.className = 'horizontal-format';
      } else {
        tag.className = 'vertical-format';
      }
    }
  }

  getConvertationsList() {
    this.messageService.getConvertationsList(this.currentUser._id).subscribe(
      resp => {
        this.convertationsList = resp.body['messages'];
        this.getUsersInfo();
      },
      error => {
        throw error;
      }
    );
  }

  getUsersInfo() {
    if (this.convertationsList) {
      this.loading = true;
      this.userIds = new Array<any>();
      this.convertationsList.forEach(convertation => {
        let userId: string;
        if (convertation["_id"].user_receiver == this.currentUser._id) {
          userId = convertation["_id"].user_sender;
        } else {
          userId = convertation["_id"].user_receiver;
        }
        if (this.userIds.length == 0) {
          this.userIds.push(userId);
        } else {
          let distinct = true;
          this.userIds.forEach(id => {
            if (id == userId) {
              distinct = false;
            }
          });
          if (distinct) {
            this.userIds.push(userId);
          }
        }
      });
      this.getPhotos();
    }
  }

  getPhotos() {
    this.convertationsListFull = new Array<any>();
    this.userIds.forEach(userId => {
      this.userService.getById(userId).subscribe(
        userResp => {
          let user: any = userResp['user'];
          this.userService.getPhoto(userId).subscribe(
            photoResp => {
              let objectURL = URL.createObjectURL(photoResp);
              user['photoUrl'] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
              this.convertationsListFull.push(user);
              this.loading = false;
            },
            error => {
              throw error;
            }
          );
        },
        error => {
          throw error;
        }
      );
    });
  }

  showMessages(user: User) {
    this.userSelected.emit(user);
  }


}
