import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { NegotiationService } from 'src/app/services/negotiation.service';
import { MessageService } from 'src/app/services/message.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { ChatService } from 'src/app/services/chat/chat.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages-dashboard',
  templateUrl: './messages-dashboard.component.html',
  styleUrls: ['./messages-dashboard.component.scss'],
  providers: [ProjectService, NegotiationService, MessageService, UserService, ChatService]
})
export class MessagesDashboardComponent implements OnInit {
  userSelected: any;
  userSelectedImage: any;
  currentUser: any;
  messageList: any
  private notifications: Subscription;

  constructor(private route: ActivatedRoute, private userService: UserService, private messageService: MessageService, private chatService: ChatService) {

  }

  ngOnInit() {
    let userId = this.route.snapshot.queryParamMap.get('id');
    this.getUser(userId);
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.notifications = this.chatService.notifications.subscribe(notification => {
      this.getMessageList();
      //this.notification = notification;
    });
  }

  getUser(id: string) {
    if (id) {
      this.userService.getById(id).subscribe(
        resp => {
          this.userSelected = resp["user"];
        },
        error => {
          throw error;
        }
      );
    }
  }

  getMessageList() {
    this.messageService.getMessages(this.userSelected._id, this.currentUser._id).subscribe(
      resp => {
        this.messageList = resp.body['messages'];
      },
      error => {
        throw error;
      }
    );
  }

  selectUser(userSelected: User) {
    this.userSelected = userSelected;
    this.getMessageList();
  }

  messageSending() {
    this.getMessageList();
  }

}
