import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingRoutingModule } from './shopping-routing.module';
import { ShoppingPageComponent } from './shopping-page/shopping-page.component';


@NgModule({
  declarations: [
    ShoppingPageComponent
  ],
  imports: [
    CommonModule,
    ShoppingRoutingModule
  ],
  bootstrap: [
    ShoppingPageComponent
  ]
})
export class ShoppingModule { }
