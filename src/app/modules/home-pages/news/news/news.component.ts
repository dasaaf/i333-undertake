import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../../../services/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  providers: [ProjectService]
})
export class NewsComponent implements OnInit {
  page: any;
  projectsList: any;
  expertsList: any;
  configCardsCollection: any;
  currentUser: User;


  constructor(private route: ActivatedRoute, private projectService: ProjectService, private userService: UserService, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (!params['page']) {
        this.page = 1;
      } else {
        this.page = params['page'];
      }
      if (this.currentUser.role[0] === "EXPERT") {
        this.getProjects();
      }
      if (this.currentUser.role[0] === "ENTREPRENEUR") {
        this.getExpertsList();
      }
    });
  }

  goToPage(pageSelected: number) {
    this.router.navigate(['/news'], { queryParams: { page: pageSelected } });
  }

  getExpertsList() {
    this.userService.getExperts(9, this.page, 'updated_date', 'desc').subscribe(
      response => {
        this.configCard();
        this.configCardsCollection.configCard.pagesNumber = response['pages_number'];
        this.expertsList = response['users'];
        this.getUsersImage();
      },
      error => {
        throw error;
      }
    )
  }

  getProjects() {
    this.projectService.getAll('null', 9, this.page, 'updated_date', 'desc').subscribe(
      response => {
        this.configCard();
        this.configCardsCollection.configCard.pagesNumber = response['pages_number'];
        this.projectsList = response['project'];
        this.getProjectsImage();
      },
      error => {
        throw error;
      }
    );
  }

  configCard() {
    this.configCardsCollection = {
      'configCard': {
        'actions': false,
        'doAQuestionAction': true,
        'actionsHoverAnimation': true,
        'descriptionHoverAnimation': true,
        'additionalInfo': true,
        'pagesNumber': 0
      }
    };
  }

  getProjectsImage() {
    var aux: any = new Array(this.projectsList.length);
    for (let i = 0; i < this.projectsList.length; i++) {
      let project = this.projectsList[i];
      this.projectService.getImage(this.projectsList[i]._id).subscribe(
        response => {
          project.image = response;
          aux[i] = project;
        },
        error => {
          throw error;
        }
      )
    }
    this.projectsList = aux;
  }

  getUsersImage() {
    var aux: any = new Array(this.expertsList.length);
    for (let i = 0; i < this.expertsList.length; i++) {
      let user = this.expertsList[i];
      this.userService.getPhoto(this.expertsList[i]._id).subscribe(
        response => {
          user.image = response;
          aux[i] = user;
        },
        error => {
          throw error;
        }
      )
    }
    this.expertsList = aux;
  }

  searchResult(resultList) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser.role[0] === "ENTREPRENEUR") {
      if (resultList && resultList.length > 0) {
        this.expertsList = resultList;
        this.getUsersImage();
      } else {
        this.page = 1;
        this.getExpertsList();
      }
    }
    if (currentUser.role[0] === "EXPERT") {
      if (resultList && resultList.length > 0) {
        this.projectsList = resultList;
        this.getProjectsImage();
      } else {
        this.page = 1;
        this.getProjects();
      }
    }
  }

  showProjectPage(project: any) {
    this.router.navigate(['/project-detail'], { queryParams: { id: project.item_selected._id } });
  }

  showCurriculumPage(user: any) {
    this.router.navigate(['/profile/expert'], { queryParams: { id: user.item_selected._id } });
  }
}
