import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news/news.component';
import { LoginModule } from '../../welcome-pages/login-page/login.module';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';
import { CardsModule } from '../../components/cards/cards.module';


@NgModule({
  declarations: [
    NewsComponent
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    LoginModule,
    CardsModule,
    MiniToolsModule
  ],
  bootstrap: [NewsComponent]
})
export class NewsModule { }
