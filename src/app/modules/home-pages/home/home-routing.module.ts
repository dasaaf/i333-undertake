import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { UndertakeComponent } from './undertake/undertake.component';

const routes: Routes = [
  {
    path: '',
    component: UndertakeComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'about', loadChildren: () => import('../../welcome-pages/about/about.module').then(m => m.AboutModule) },
      { path: 'change-pass', loadChildren: () => import('../../welcome-pages/change-pass/change-pass.module').then(m => m.ChangePassModule) },
      { path: 'profile', loadChildren: () => import('../../home-pages/profile/profile.module').then(m => m.ProfileModule) },
      { path: 'edit-project', loadChildren: () => import('../../home-pages/edit-project/edit-project.module').then(m => m.EditProjectModule) },
      { path: 'frecuent-questions', loadChildren: () => import('../../welcome-pages/frecuent-questions/frecuent-questions.module').then(m => m.FrecuentQuestionsModule) },
      { path: 'help', loadChildren: () => import('../../welcome-pages/help/help.module').then(m => m.HelpModule) },
      { path: 'messages', loadChildren: () => import('../messages/messages.module').then(m => m.MessagesModule) },
      { path: 'news', loadChildren: () => import('../news/news.module').then(m => m.NewsModule) },
      { path: 'new-project', loadChildren: () => import('../new-project/new-project.module').then(m => m.NewProjectModule) },
      { path: 'projects', loadChildren: () => import('../projects/projects.module').then(m => m.ProjectsModule) },
      { path: 'project-dashboard', loadChildren: () => import('../project-dashboard/project-dashboard.module').then(m => m.ProjectDashboardModule) },
      { path: 'project-detail', loadChildren: () => import('../project-detail/project-detail.module').then(m => m.ProjectDetailModule) },
      { path: 'shopping', loadChildren: () => import('../shopping/shopping.module').then(m => m.ShoppingModule) },
      { path: 'work-with-us', loadChildren: () => import('../../welcome-pages/work-with-us/work-with-us.module').then(m => m.WorkWithUsModule) },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
