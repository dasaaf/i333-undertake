import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { UndertakeComponent } from './undertake/undertake.component';
import { MenuComponent } from './menu/menu.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';


@NgModule({
  declarations: [
    UndertakeComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MiniToolsModule
  ],
  bootstrap: [
    UndertakeComponent
  ]
})
export class HomeModule { }
