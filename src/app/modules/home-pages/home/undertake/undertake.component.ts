import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat/chat.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-undertake',
  templateUrl: './undertake.component.html',
  styleUrls: ['./undertake.component.scss'],
  providers: [UserService, ChatService, StorageSessionService],
})
export class UndertakeComponent implements OnInit {
  private notifications: Subscription;
  notification: any;
  currentUser: User;
  userImage: any;
  showMenu: boolean;
  section: string;
  imageStyle: string;

  constructor(private sanitizer: DomSanitizer, private userService: UserService, private storageSessionService: StorageSessionService, private router: Router, private chatService: ChatService) {
    this.showMenu = false;
    router.events.subscribe((val) => {
      let url = this.router.url.split('?')[0];
      this.section = url;
      if (url == "/") {
        this.router.navigate(['/news']);
      }
    });
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.getUserPhoto();
    this.storageSessionService.watchStorage().subscribe((data: string) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.getUserPhoto();
    });
    this.chatService.joinRoom(this.currentUser._id);
    this.notifications = this.chatService.notifications.subscribe(notification => {
      this.notification = notification;
    });
  }

  getUserPhoto() {
    if (this.currentUser && this.currentUser._id) {
      this.userService.getPhoto(this.currentUser._id).subscribe(
        response => {
          let objectURL = URL.createObjectURL(response);
          this.userImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  mobileMenu() {
    this.showMenu = !this.showMenu;
  }

  logout() {
    this.storageSessionService.removeItem('currentUser');
    this.storageSessionService.removeItem('token');
    this.router.navigate(['/login']);
  }

  setStyleImage(dimension: any) {
    if (dimension.width > dimension.height) {
      this.imageStyle = 'horizontal-format';
    } else {
      this.imageStyle = 'vertical-format';
    }
  }

}
