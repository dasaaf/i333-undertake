import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageSessionService } from 'src/app/services/storage-session.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [StorageSessionService, StorageSessionService]
})
export class MenuComponent implements OnInit {
  @Input() notification: any;
  section: String;

  constructor(private router: Router, private storageSessionService: StorageSessionService) {
    router.events.subscribe((val) => {
      let url = this.router.url.split('?')[0];
      this.section = url;
    });
  }

  ngOnInit() {
  }

  logout() {
    this.storageSessionService.removeItem('currentUser');
    this.storageSessionService.removeItem('token');
    this.router.navigate(['/login']);
  }

}
