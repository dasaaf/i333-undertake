import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectDashboardRoutingModule } from './project-dashboard-routing.module';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';


@NgModule({
  declarations: [
    ProjectDashboardComponent
  ],
  imports: [
    CommonModule,
    ProjectDashboardRoutingModule,
    MiniToolsModule
  ],
  bootstrap: [ProjectDashboardComponent]
})
export class ProjectDashboardModule { }
