import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-dashboard',
  templateUrl: './project-dashboard.component.html',
  styleUrls: ['./project-dashboard.component.scss'],
  providers: [ProjectService]
})
export class ProjectDashboardComponent implements OnInit {
  project: Project;
  projectImageUrl: any;

  constructor(private route: ActivatedRoute, private projectService: ProjectService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    let id = this.route.snapshot.queryParamMap.get('id');
    if (id) {
      this.getProject(id);
      this.getProjectImage(id);
    }
  }

  getProject(id: string) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.projectService.getById(id).subscribe(
      resp => {
        if(resp['project'].user == currentUser._id){
          this.project = resp['project'];
        }
      },
      error => {
        throw error;
      }
    );
  }

  getProjectImage(id: string) {
    this.projectService.getImage(id).subscribe(
      resp => {
        let url = URL.createObjectURL(resp);
        this.projectImageUrl = this.sanitizer.bypassSecurityTrustUrl(url);
      },
      error => {
        throw error;
      }
    );
  }
}
