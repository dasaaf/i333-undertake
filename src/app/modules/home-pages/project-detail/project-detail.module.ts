import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectDetailRoutingModule } from './project-detail-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';

@NgModule({
  declarations: [
    ProjectDetailComponent
  ],
  imports: [
    CommonModule,
    ProjectDetailRoutingModule,
    MiniToolsModule
  ],
  bootstrap: [ProjectDetailComponent]
})
export class ProjectDetailModule { }
