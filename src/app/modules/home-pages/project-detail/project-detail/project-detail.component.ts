import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RatingService } from 'src/app/services/rating.service';
import { UserService } from 'src/app/services/user.service';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { Location } from 'src/app/models/project';
import { Rating } from 'src/app/models/rating';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { LocationService } from 'src/app/services/location.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
  providers: [RatingService, UserService, StorageSessionService, ProjectService, LocationService]
})
export class ProjectDetailComponent implements OnInit {
  currentUser: any;
  projectImageUrl: any;
  userPhoto: any;
  negotiationMessage: boolean;
  userDetail: any;
  rating: Rating;
  score: number;
  project: Project;
  projectProperty: User;
  imageStyle: string;

  constructor(private route: ActivatedRoute, private router: Router, private projectService: ProjectService, private storageSessionService: StorageSessionService, private userService: UserService, private sanitizer: DomSanitizer, private locationService: LocationService) {
  }

  ngOnInit() {
    let id = this.route.snapshot.queryParamMap.get('id');
    this.negotiationMessage = false;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.storageSessionService.watchStorage().subscribe((data: string) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    });
    this.getProject(id);
  }

  getProject(id: string) {
    this.projectService.getById(id).subscribe(
      resp => {
        this.project = resp['project'];
        this.getProjectImage();
        this.getProjectProperty(this.project.user);
        this.getLocations();
        this.getUserPhoto();
      },
      error => {
        throw error;
      }
    );
  }

  getLocations() {
    let projectLocations = this.project.locations;
    this.project.locations = new Array();
    projectLocations.forEach(location => {
      let newLocation: Location = new Location();
      this.locationService.getCountry(location.country).subscribe(
        response => {
          newLocation.country = response['name'];
          this.locationService.getState(location.state).subscribe(
            response => {
              newLocation.state = response['name'];
              this.locationService.getCity(location.city).subscribe(
                response => {
                  newLocation.city = response['name'];
                  this.project.locations.push(newLocation);
                },
                error => {
                  throw error;
                }
              )
            },
            error => {
              throw error;
            }
          )
        },
        error => {
          throw error;
        }
      );
    });
  }

  startNegotiation(project: any) {
    if (this.currentUser._id == this.projectProperty._id) {
      this.negotiationMessage = true;
    } else {
      this.router.navigate(['/messages'], { queryParams: { id: project.user } });
    }
  }

  closeNegotiationMessage() {
    this.negotiationMessage = false;
  }

  getUserPhoto() {
    this.userService.getPhoto(this.project.user).subscribe(
      response => {
        let objectURL = URL.createObjectURL(response);
        this.userPhoto = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      error => {
        throw error;
      }
    );
  }

  getProjectImage() {
    this.projectService.getImage(this.project._id).subscribe(
      resp => {
        let objectURL = URL.createObjectURL(resp);
        this.projectImageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      error => {
        throw error;
      }
    );
  }

  getProjectProperty(userId: string) {
    this.userService.getById(userId).subscribe(
      resp => {
        this.projectProperty = resp['user'];
      },
      error => {
        throw error;
      }
    )
  }

  setStyleImage(dimension: any) {
    if (dimension.width >= dimension.height) {
      this.imageStyle = 'horizontal-format';
    } else {
      this.imageStyle = 'vertical-format';
    }
  }
}
