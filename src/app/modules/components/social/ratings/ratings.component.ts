import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.component.html',
  styleUrls: ['./ratings.component.scss']
})
export class RatingsComponent implements OnInit {
  @Input() rating: number;
  @Output() qualification = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  qualify(value: number) {
    this.qualification.emit(value);
  }

}
