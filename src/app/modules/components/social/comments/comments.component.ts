import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment';
import { CommentService } from 'src/app/services/comment.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  providers: [CommentService, UserService]
})
export class CommentsComponent implements OnInit, OnChanges {
  @Input() commentator: User;
  @Input() expert: User;
  commentatorPhoto: any;
  textComment: string;
  labelDescription: string;
  commentsListPublic: boolean;
  commentsList: any;
  more: any;
  showErrorMessage: boolean;

  constructor(private commentService: CommentService, private userService: UserService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.initText();
    this.commentsListPublic = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.commentator && changes.commentator.currentValue) {
        this.commentator = changes.commentator.currentValue;
        this.getCommentatorPhoto();
      }
      if (changes.expert && changes.expert.currentValue) {
        this.expert = changes.expert.currentValue;
        this.getCommentList();
      }
    }
  }

  getCommentatorPhoto() {
    this.userService.getPhoto(this.commentator._id).subscribe(
      response => {
        let objectURL = URL.createObjectURL(response);
        this.commentatorPhoto = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      error => {
        throw error;
      }
    );
  }

  updateComment(text: any) {
    if (text.length > 255) {
      this.showErrorMessage = true;
    } else {
      this.textComment = text;
      this.showErrorMessage = false;
    }
  }

  clearText() {
    this.labelDescription = "";
  }

  initText() {
    if (this.textComment == null || this.textComment == "") {
      this.labelDescription = "Pública un comentario";
    }
  }

  showMore(commentId: any) {
    if (this.more != commentId) {
      this.more = commentId;
    } else {
      this.more = null;
    }
  }

  getCommentList() {
    this.commentService.getCommentList(this.expert._id).subscribe(
      response => {
        this.commentsList = response['comments'];
        this.commentsList.forEach(comment => {
          this.userService.getPhoto(comment.commentator._id).subscribe(
            response => {
              let objectURL = URL.createObjectURL(response);
              let userPhoto = this.sanitizer.bypassSecurityTrustUrl(objectURL);
              comment['user_photo'] = userPhoto;
            },
            error => {
              throw error;
            }
          );
        });
      },
      error => {
        throw error;
      }
    );
  }

  postComment(input: any) {
    if (!this.showErrorMessage) {
      let comment: Comment = new Comment();
      comment.commentator = this.commentator._id;
      comment.expert = this.expert._id;
      comment.description = this.textComment;
      this.commentService.create(comment).subscribe(
        response => {
          this.getCommentList();
          this.textComment = "";
          input.innerText = 'Pública un comentario';
        },
        error => {
          throw error;
        }
      );

    }
  }

}
