import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsComponent } from './comments/comments.component';
import { RatingsComponent } from './ratings/ratings.component';



@NgModule({
  declarations: [
    CommentsComponent,
    RatingsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommentsComponent,
    RatingsComponent
  ]
})
export class SocialModule { }
