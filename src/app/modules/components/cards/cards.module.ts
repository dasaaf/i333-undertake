import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { CardsCollectionComponent } from './cards-collection/cards-collection.component';
import { MiniToolsModule } from '../mini-tools/mini-tools.module';



@NgModule({
  declarations: [
    CardComponent,
    CardsCollectionComponent,
  ],
  imports: [
    CommonModule,
    MiniToolsModule
  ],
  exports: [
    CardComponent,
    CardsCollectionComponent
  ]
})
export class CardsModule { }
