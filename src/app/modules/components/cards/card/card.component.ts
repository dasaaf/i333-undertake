import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() config: any;
  @Output() deleteRecord = new EventEmitter<any>();
  @Output() showContent = new EventEmitter<any>();

  imgSrcDelete: string;
  imgSrcShare: string;
  imgSrcQuestion: string;

  image: any;
  imageStyle: string;

  constructor(private sanitizer: DomSanitizer) {
    this.imgSrcDelete = 'assets/images/trash-icon.png';
    this.imgSrcShare = 'assets/images/share-icon.png';
    this.imgSrcQuestion = 'assets/images/questions-icon.png';
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && changes.data.currentValue) {
      this.data = changes.data.currentValue;
    }
    if (changes.config && changes.config.currentValue) {
      this.config = changes.config.currentValue;
    }
  }

  ngOnInit() {
    if (this.data && this.data.image) {
      let objectURL = URL.createObjectURL(this.data.image);
      this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }
  }

  show() {
    this.showContent.emit({ 'action': 'view', 'item_selected': this.data });
  }

  deleteCard() {
    this.deleteRecord.emit({ 'action': 'delete', 'item_selected': this.data });
  }

  setStyleImage(dimension: any) {
    if (dimension.width > dimension.height) {
      this.imageStyle = 'horizontal-format';
    } else {
      this.imageStyle = 'vertical-format';
    }
  }
}
