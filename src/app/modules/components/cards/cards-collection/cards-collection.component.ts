import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-cards-collection',
  templateUrl: './cards-collection.component.html',
  styleUrls: ['./cards-collection.component.scss']
})
export class CardsCollectionComponent implements OnInit, OnChanges {
  @Input() cards: any;
  @Input() config: any;
  @Input() page: any;
  @Output() deleteRecord = new EventEmitter<any>();
  @Output() showCardInfo = new EventEmitter<any>();
  @Output() pageSelected = new EventEmitter<any>();
  topLimit: number;
  bottomLimit: number;
  pagesNumber: number;
  pages: any;

  constructor() { }

  ngOnInit() {
    this.pages = new Array();
    this.topLimit = 9;
    this.bottomLimit = 1;
    this.chargePages();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.cards && changes.cards.currentValue) {
      this.cards = changes.cards.currentValue;
    }
    if (changes.config) {
      this.config = changes.config.currentValue;
      this.chargePages();
    }
    if (changes.page && changes.page.currentValue) {
      this.page = changes.page.currentValue;
    }
  }

  sendEventDelete(event: any) {
    this.deleteRecord.emit(event);
  }

  show(event: any) {
    this.showCardInfo.emit(event);
  }

  selectPage(page: any) {
    this.pageSelected.emit(page);
    this.page = page;
  }

  chargePages() {
    this.pagesNumber = this.config.configCard.pagesNumber;
    if (this.pagesNumber > 0) {
      this.pages = new Array();
      for (let i = this.bottomLimit; i <= this.pagesNumber && i <= this.topLimit; i++) {
        this.pages.push(i);
      }
    }
  }

  next() {
    if (this.topLimit < this.pagesNumber) {
      this.bottomLimit = this.topLimit;
      this.topLimit = this.topLimit + 9;
      this.page = this.bottomLimit;
      this.chargePages();
      this.pageSelected.emit(this.page);
    }
  }

  previous() {
    if (this.bottomLimit > 8) {
      this.topLimit = this.bottomLimit;
      if (this.bottomLimit == 9) {
        this.bottomLimit = this.bottomLimit - 8;
      } else {
        this.bottomLimit = this.bottomLimit - 9;
      }
      this.page = this.topLimit;
      this.chargePages();
      this.pageSelected.emit(this.page);
    }
  }

}
