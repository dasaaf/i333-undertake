import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {
  @Output() resultsListEvent = new EventEmitter<any>();
  resultsList: any;
  currentUser: any;

  constructor(private searchService: SearchService) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  ngOnInit() {
  }

  search(prediction?: any, searchField?: any) {
    let text = "";
    if (this.currentUser.role[0] === "ENTREPRENEUR") {
      if (prediction && prediction.first_name) {
        text = prediction.first_name;
        searchField.value = prediction.first_name + " " + prediction.last_name + ", " + prediction.title;
      } else {
        text = searchField.value;
      }
      this.searchService.searchExpert(text, 9, 1).subscribe(
        result => {
          this.resultsList = "";
          this.resultsListEvent.emit(result['user']);
        },
        error => {
          throw error;
        }
      );
    } else if (this.currentUser.role[0] === "EXPERT") {
      if (prediction && prediction.title) {
        text = prediction.title;
        searchField.value = prediction.title;
      } else {
        text = searchField.value;
      }
      this.searchService.searchProject(text, 9, 1).subscribe(
        result => {
          this.resultsList = "";
          this.resultsListEvent.emit(result['project']);
        },
        error => {
          throw error;
        }
      );
    }
  }

  autoSearch(text: string) {
    if (text && text != "") {
      if (this.currentUser.role[0] === "ENTREPRENEUR") {
        this.searchService.searchExpert(text, 9, 1).subscribe(
          result => {
            this.resultsList = result['user'];
            this.resultsListEvent.emit(this.resultsList);
          },
          error => {
            throw error;
          }
        );
      } else if (this.currentUser.role[0] === "EXPERT") {
        this.searchService.searchProject(text, 9, 1).subscribe(
          result => {
            this.resultsList = result['project'];
            this.resultsListEvent.emit(this.resultsList);
          },
          error => {
            throw error;
          }
        );
      }
    } else {
      this.resultsList = new Array();
    }
  }

}
