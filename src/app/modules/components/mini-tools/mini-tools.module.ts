import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingResourceComponent } from './loading-resource/loading-resource.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { SearchComponent } from './search/search.component';
import { ImageDimensionDirective } from './directives/image-dimension/image-dimension.directive';



@NgModule({
  declarations: [
    LoadingResourceComponent,
    LoadingScreenComponent,
    SearchComponent,
    ImageDimensionDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LoadingResourceComponent,
    LoadingScreenComponent,
    SearchComponent,
    ImageDimensionDirective
  ]
})
export class MiniToolsModule { }
