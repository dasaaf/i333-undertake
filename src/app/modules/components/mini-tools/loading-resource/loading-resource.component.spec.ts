import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingResourceComponent } from './loading-resource.component';

describe('LoadingResourceComponent', () => {
  let component: LoadingResourceComponent;
  let fixture: ComponentFixture<LoadingResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
