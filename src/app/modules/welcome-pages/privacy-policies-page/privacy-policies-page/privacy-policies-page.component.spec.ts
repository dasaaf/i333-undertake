import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyPoliciesPageComponent } from './privacy-policies-page.component';

describe('PrivacyPoliciesPageComponent', () => {
  let component: PrivacyPoliciesPageComponent;
  let fixture: ComponentFixture<PrivacyPoliciesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyPoliciesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyPoliciesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
