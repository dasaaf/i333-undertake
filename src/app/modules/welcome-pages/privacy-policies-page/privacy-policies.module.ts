import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyPoliciesRoutingModule } from './privacy-policies-routing.module';
import { PrivacyPoliciesPageComponent } from './privacy-policies-page/privacy-policies-page.component';
import { RegistryModule } from '../registry-page/registry.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PrivacyPoliciesPageComponent
  ],
  imports: [
    CommonModule,
    PrivacyPoliciesRoutingModule,
    RegistryModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [PrivacyPoliciesPageComponent]
})
export class PrivacyPoliciesModule { }
