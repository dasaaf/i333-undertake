import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivacyPoliciesPageComponent } from './privacy-policies-page/privacy-policies-page.component';


const routes: Routes = [
  { path: '', component: PrivacyPoliciesPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivacyPoliciesRoutingModule { }
