import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../../../../models/user';
import { UserService } from '../../../../services/user.service';
import { MeService } from '../../../../services/me.service';
import { StorageSessionService } from 'src/app/services/storage-session.service';
import { LoginLinkedinService } from 'src/app/services/login-linkedin.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: [UserService, StorageSessionService, LoginLinkedinService, MeService]
})

export class LoginFormComponent implements OnInit {
  model: User;
  returnUrl: string;
  loginForm: FormGroup;
  message: any;
  sendRequest: boolean = false;

  constructor(private route: ActivatedRoute, private userService: UserService, private loginLinkedInService: LoginLinkedinService, private storageSession: StorageSessionService, private router: Router, formBuilder: FormBuilder, private meService: MeService) {
    this.route.queryParams.subscribe(params => {
      if (params.access) {
        this.me(params.access);
      }
    });
    this.model = new User();
    this.loginForm = formBuilder.group({
      email: new FormControl(this.model.email, [Validators.required, Validators.email]),
      password: new FormControl(this.model.password, [Validators.required]),
    });
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['/news']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/news';
    }
  }

  loginWithLinkedin() {
    location.href = this.loginLinkedInService.login();
    //this.router.navigateByUrl(this.loginLinkedInService.login());
    /*this.router.navigate(['/externalRedirect', { externalUrl: this.loginLinkedInService.login() }], {
      skipLocationChange: true,
  });*/
    //this.router.navigate([this.loginLinkedInService.login()]);
  }

  me(token) {
    this.meService.getMe(token)
      .subscribe(
        resp => {
          if (resp.body.message) {
            this.message = resp.body.message.text;
          } else if (resp.body.user) {
            resp.body.user.password = "";
            this.storageSession.setItem('token', token);
            this.storageSession.setItem('currentUser', JSON.stringify(resp.body.user));
            this.router.navigate([this.returnUrl]);
          }
        },
        error => {
          throw new Error(error);
        }
      );
  }

  login() {
    this.sendRequest = true;
    if (this.loginForm.valid) {
      this.sendRequest = false;
      this.model.email = this.loginForm.value.email;
      this.model.password = this.loginForm.value.password;
      this.userService.login(this.model.email, this.model.password)
        .subscribe(
          resp => {
            if (resp.body.message) {
              this.message = resp.body.message.text;
              if (this.message === "User or password is not valid") {
                this.message = "El usuario o la contraseña no es válida";
              }
              if (this.message === "Please, activate your account from the mail sended") {
                this.message = "Por favor activa tu cuenta desde el mensaje enviado a tu correo electrónico.";
              }
            } else if (resp.body.user) {
              resp.body.user.password = "";
              this.storageSession.setItem('token', resp.headers.get('token'));
              this.storageSession.setItem('currentUser', JSON.stringify(resp.body.user));
              this.router.navigate([this.returnUrl]);
            }
          },
          error => {
            console.log(error);
            if (error && error.error) {
              switch (error.error.message.code) {
                case 404:
                  this.message = "El usuario no se encuentra registrado";
                  break;
                default:
                  throw new Error(error);
              }
            }
          }
        );
    }
  }

}
