import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginPageComponent } from './login/login-page.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { LinkedinComponent } from './linkedin/linkedin.component';
import { MiniToolsModule } from '../../components/mini-tools/mini-tools.module';


@NgModule({
  declarations: [
    LoginPageComponent,
    LoginFormComponent,
    LinkedinComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MiniToolsModule
  ],
  exports: [
    LoginFormComponent
  ],
  bootstrap: [LoginPageComponent]
})
export class LoginModule { }
