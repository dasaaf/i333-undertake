import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinkedinComponent } from './linkedin/linkedin.component';
import { LoginPageComponent } from './login/login-page.component';


const routes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'linkedin', component: LinkedinComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
