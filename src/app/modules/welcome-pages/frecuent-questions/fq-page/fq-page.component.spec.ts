import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FqPageComponent } from './fq-page.component';

describe('FqPageComponent', () => {
  let component: FqPageComponent;
  let fixture: ComponentFixture<FqPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FqPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FqPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
