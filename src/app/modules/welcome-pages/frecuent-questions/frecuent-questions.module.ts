import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrecuentQuestionsRoutingModule } from './frecuent-questions-routing.module';
import { FqPageComponent } from './fq-page/fq-page.component';


@NgModule({
  declarations: [
    FqPageComponent
  ],
  imports: [
    CommonModule,
    FrecuentQuestionsRoutingModule
  ],
  bootstrap: [FqPageComponent]
})
export class FrecuentQuestionsModule { }
