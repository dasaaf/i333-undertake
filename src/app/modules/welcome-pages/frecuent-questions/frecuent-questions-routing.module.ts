import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FqPageComponent } from './fq-page/fq-page.component';

const routes: Routes = [
  { path: '', component: FqPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrecuentQuestionsRoutingModule { }
