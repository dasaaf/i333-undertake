import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-password-recovery-form',
  templateUrl: './password-recovery-form.component.html',
  styleUrls: ['./password-recovery-form.component.scss'],
  providers: [UserService]
})
export class PasswordRecoveryFormComponent implements OnInit {
  model: User;
  recoveryPassForm: FormGroup;
  returnUrl: string;
  message: string;
  reCaptchaValid: boolean;

  constructor(formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.model = new User();
    this.recoveryPassForm = formBuilder.group({
      email: new FormControl(this.model.email, [Validators.required, Validators.email]),
      captcha: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['/']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
  }

  recoveryPass() {
    let passRecovery: any = { email: this.recoveryPassForm.value.email };
    if (this.recoveryPassForm.valid && this.reCaptchaValid) {
      this.userService.passwordRecovery(passRecovery).subscribe(
        resp => {
          if (resp['message'] === "The user doesn't exists") {
            this.message = "Parece que el correo que has ingresado ¡No está registrado!";
          } else {
            this.message = "¡Hemos enviado un mensaje a tu correo, por favor verifica!";
          }
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.message = "Por favor completa todos los campos del formulario";
    }
  }

  verifyRecaptcha(captchaResolve) {
    let captcha: any = { captcha: captchaResolve };
    this.userService.verifyRecaptcha(captcha).subscribe(
      resp => {
        if (resp['success']) {
          this.reCaptchaValid = true;
        }
      },
      error => {
        throw new Error(error);
      }
    )
  }

  closeMessage() {
    this.message = null;
  }

}
