import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { PasswordValidator } from 'src/app/validations/password-validator';

@Component({
  selector: 'app-password-reset-form',
  templateUrl: './password-reset-form.component.html',
  styleUrls: ['./password-reset-form.component.scss'],
  providers: [UserService]
})
export class PasswordResetFormComponent implements OnInit {
  @Output() passChange = new EventEmitter<boolean>();
  model: User;
  resetPassForm: FormGroup;
  errorMessage: boolean = false;
  currentUser: User;

  constructor(formBuilder: FormBuilder, private userService: UserService, private route: ActivatedRoute) {
    this.model = new User();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let actual_pass_validator = [];
    if (this.currentUser) {
      actual_pass_validator = [Validators.required];
    }
    this.resetPassForm = formBuilder.group({
      actual_pass: new FormControl(this.model.password, actual_pass_validator),
      password: new FormControl("", [Validators.required]),
      confirm_password: new FormControl('', [Validators.required]),
    },
      {
        validator: PasswordValidator.MatchPassword
      });
  }

  ngOnInit() {
  }

  resetPass() {
    if (this.resetPassForm.valid) {
      let passwordRecoveryToken = this.route.snapshot.queryParamMap.get('pass');
      let userId = this.route.snapshot.queryParamMap.get('user');
      let data = {};
      if (passwordRecoveryToken && userId) {
        data = { user_id: userId, pass_token: passwordRecoveryToken, new_pass: this.resetPassForm.value.password };
      }
      if (this.currentUser && this.resetPassForm.value.actual_pass) {
        data = { user_id: this.currentUser._id, actual_pass: this.resetPassForm.value.actual_pass, new_pass: this.resetPassForm.value.password };
      }
      this.userService.resetPassword(data).subscribe(
        resp => {
          if (resp['change']) {
            this.passChange.emit(true);
          } else {
            this.errorMessage = true;
          }
        },
        error => {
          throw new Error(error);
        }
      );
    }
  }

}
