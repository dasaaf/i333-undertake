import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss'],
  providers: [UserService]
})
export class PasswordRecoveryComponent implements OnInit {
  showPassChangeMessage: boolean = false;
  passReset: boolean = false;
  user: User;
  data: any;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    let passwordRecoveryToken = this.route.snapshot.queryParamMap.get('pass');
    let userId = this.route.snapshot.queryParamMap.get('user');
    this.showPassChangeMessage = false;
    if (passwordRecoveryToken && userId) {
      this.passReset = true;
      this.getUser(userId);
    }
  }

  getUser(userId: any) {
    this.userService.getById(userId).subscribe(
      resp => {
        this.user = resp['user'];
      },
      error => {
        console.log(error);
      }
    )
  }

  showPassChange(passChange: boolean) {
    if (passChange) {
      this.showPassChangeMessage = true;
    }
  }

}
