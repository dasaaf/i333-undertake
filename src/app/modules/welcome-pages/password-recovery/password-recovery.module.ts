import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';
import { PasswordRecoveryFormComponent } from './password-recovery-form/password-recovery-form.component';
import { PasswordResetFormComponent } from './password-reset-form/password-reset-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  declarations: [
    PasswordRecoveryComponent,
    PasswordRecoveryFormComponent,
    PasswordResetFormComponent
  ],
  imports: [
    CommonModule,
    PasswordRecoveryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  exports: [
    PasswordResetFormComponent
  ],
  bootstrap: [
    PasswordRecoveryComponent
  ]
})
export class PasswordRecoveryModule { }
