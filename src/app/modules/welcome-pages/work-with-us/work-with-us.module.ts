import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkWithUsRoutingModule } from './work-with-us-routing.module';
import { WorkWithUsComponent } from './work-with-us/work-with-us.component';
import { ContactModule } from '../contact-page/contact.module';


@NgModule({
  declarations: [
    WorkWithUsComponent
  ],
  imports: [
    CommonModule,
    WorkWithUsRoutingModule,
    ContactModule
  ],
  bootstrap: [WorkWithUsComponent]
})
export class WorkWithUsModule { }
