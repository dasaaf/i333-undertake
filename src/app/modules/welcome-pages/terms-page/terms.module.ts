import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsRoutingModule } from './terms-routing.module';
import { TermsPageComponent } from './terms/terms-page.component';
import { RegistryModule } from '../registry-page/registry.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TermsPageComponent
  ],
  imports: [
    CommonModule,
    TermsRoutingModule,
    RegistryModule,
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [TermsPageComponent]
})
export class TermsModule { }
