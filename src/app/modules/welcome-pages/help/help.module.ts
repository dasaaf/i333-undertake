import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpRoutingModule } from './help-routing.module';
import { HelpPageComponent } from './help-page/help-page.component';
import { ContactModule } from '../contact-page/contact.module';


@NgModule({
  declarations: [
    HelpPageComponent
  ],
  imports: [
    CommonModule,
    HelpRoutingModule,
    ContactModule
  ],
  bootstrap: [
    HelpPageComponent
  ]
})
export class HelpModule { }
