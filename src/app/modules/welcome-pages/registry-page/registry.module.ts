import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistryPageComponent } from './registry-page/registry-page.component';
import { RegistryFormComponent } from './registry-form/registry-form.component';
import { RegistryRoutingModule } from './registry-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RegistryFormComponent,
    RegistryPageComponent,
  ],
  imports: [
    CommonModule,
    RegistryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    RegistryFormComponent,
  ],
  bootstrap: [RegistryPageComponent]
})
export class RegistryModule { }
