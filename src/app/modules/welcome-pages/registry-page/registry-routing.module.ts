import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistryPageComponent } from './registry-page/registry-page.component';


const routes: Routes = [
  { path: '', component: RegistryPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistryRoutingModule { }
