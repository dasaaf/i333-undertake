import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PasswordValidator } from '../../../../validations/password-validator';
import { User } from '../../../../models/user';

@Component({
  selector: 'app-registry-form',
  templateUrl: './registry-form.component.html',
  styleUrls: ['./registry-form.component.scss'],
  providers: [UserService]
})
export class RegistryFormComponent implements OnInit {
  model: User;
  registryForm: FormGroup;
  message: string;

  constructor(private router: Router, private userService: UserService, formBuilder: FormBuilder) {
    this.model = new User();
    this.registryForm = formBuilder.group({
      email: new FormControl(this.model.email, [Validators.required, Validators.email]),
      first_name: new FormControl(this.model.first_name, [Validators.required]),
      last_name: new FormControl(this.model.last_name, [Validators.required]),
      password: new FormControl(this.model.password, [Validators.required, Validators.minLength(9), PasswordValidator.patternValidator(/\d/, { hasNumber: true }), PasswordValidator.patternValidator(/[A-Z]/, { hasCapitalCase: true })]),
      confirm_password: new FormControl('', [Validators.required]),
    }, {
      validator: PasswordValidator.MatchPassword
    }
    );
  }

  ngOnInit() {

  }

  registry() {
    console.log("Carga");
    if (this.registryForm.valid) {
      this.model.email = this.registryForm.value.email;
      this.model.first_name = this.registryForm.value.first_name;
      this.model.last_name = this.registryForm.value.last_name;
      this.model.password = this.registryForm.value.password;
      this.userService.create(this.model)
        .subscribe(
          data => {
            if (data["message"] === "User already exists, please login with your email and password") {
              this.message = "user-exits";
            } else if (data["message"] === "We have sent a mail for activate your account") {
              this.message = "sended-mail";
            }
          },
          error => {
            console.log(error);
          });
    }
  }

  closeMessage() {
    this.message = null;
  }
}
