import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePassRoutingModule } from './change-pass-routing.module';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordRecoveryModule } from '../password-recovery/password-recovery.module';


@NgModule({
  declarations: [
    ChangePassComponent
  ],
  imports: [
    CommonModule,
    ChangePassRoutingModule,
    PasswordRecoveryModule,
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [ChangePassComponent]
})
export class ChangePassModule { }
