import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageSessionService } from 'src/app/services/storage-session.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit {

  constructor(private router: Router, private storageSessionService: StorageSessionService) { }

  ngOnInit() {
  }

  passChange(change: boolean) {
    console.log(change);
    if (change) {
      this.storageSessionService.removeItem('currentUser');
      this.storageSessionService.removeItem('token');
      this.router.navigate(['/login']);
    }
  }

}
