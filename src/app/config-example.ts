export const Config = {
    URL: 'http://127.0.0.1:3000/api',
    IP: 'localhost',
    SOCKET_ENDPOINT: 'ws://192.168.0.16:5001',
    MIME_TYPES: {
        pdf: 'application/pdf'
    }
}
