import { Message } from "./message";

export class Negotiation {
    _id?: string;
    state: string;
    created_date?: Date;
    updated_date?: Date;
    deleted_date?: Date;
    project: string;
    interested_user: string;
    property_user: string;
    message?: Message;
}
