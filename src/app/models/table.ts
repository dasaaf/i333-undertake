export class tableConfig {
    columns?: [];
    actions?: tableActions;
    pagesNumber?: number;
    page?: number;
}

export class tableActions {
    add: actionButton;
    edit: actionButton;
    delete: actionButton;
    see: actionButton;
    custom: []
}

export class actionButton {
    tag: String;
    enabled: boolean;
}