﻿export class Rating {
    _id?: string;
    qualification?: Number;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date;
    grader: String;
    expert: String;
}