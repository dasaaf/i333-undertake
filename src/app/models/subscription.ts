export class Subscription {
    _id?: string;
    user: string;
    state: string;
    price: number;
    duration: number;
    discount: number;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date
}