
export class Investment {
    _id?: string;
    state?: string;
    created_date?: Date;
    updated_date?: Date;
    deleted_date?: Date;
    project: string;
    investor_user: string;
}