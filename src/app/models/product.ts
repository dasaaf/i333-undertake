export class Product {
    _id?: string;
    product_images: [any];
    name: string;
    description: string;
    type: string;
    step_duration?: string;
    duration?: string;
    state: string;
    price: number;
    discount: number;
    sales?: number;
    orders?: number;
    published?: boolean;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date
}