﻿export class Comment {
    _id?: string;
    state?: string;
    description?: string;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date;
    commentator?: string;
    expert?: string;
}