export class Project {
    _id?: string;
    project_image: string;
    title?: string;
    description?: string;
    needs?: string;
    locations?: Array<Location>;
    swot_matrix?: SwotMatrix;
    business_model_canvas?: BusinessModelCanvas;
    state: string;
    user: string;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date
}

export class SwotMatrix {
    strengths: Array<{ description: string }>;
    weaknesses: Array<{ description: string }>;
    opportunities: Array<{ description: string }>;
    threats: Array<{ description: string }>;
}

export class BusinessModelCanvas {
    value_propositions: Array<{ description: string }>;
    key_partners: Array<{ description: string }>;
    customer_segments: Array<{ description: string }>;
    key_activities: Array<{ description: string }>;
    key_resources: Array<{ description: string }>;
    customer_relationships: Array<{ description: string }>;
    channels: Array<{ description: string }>;
    cost_structure: Array<{ description: string }>;
    revenue_streams: Array<{ description: string }>;
}

export class Location {
    country: string;
    state: string;
    city: string;
}