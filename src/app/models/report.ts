﻿export class Report {
    _id?: string;
    description?: string;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date;
}