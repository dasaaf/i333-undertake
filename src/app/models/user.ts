﻿export class User {
    _id?: string;
    first_name?: string;
    last_name?: string;
    title?: string;
    photo?: string;
    professional_profile?: string;
    academic_training?: [AcademicTraining];
    laboral_experiences?: [Experience];
    skills?: [Skill];
    references?: [Reference];
    certificates: [Document];
    email: string;
    password: string;
    state: string;
    role: [String];
    birth_date?: Date;
    created_date: Date;
    updated_date: Date;
    deleted_date?: Date
}

export class AcademicTraining {
    academic_title: string;
    institution: string;
    state: string;
    start_date: Date;
    end_date: Date;
}

export class Document {
    id: string;
    key: string;
    name: string;
}

export class Skill {
    name: string;
    level: number;
}

export class Experience {
    institution: string;
    position: string;
    description: string;
    start_date: Date;
    end_date: Date;
}

export class Reference {
    first_name: string;
    last_name: string;
    position: string;
    title: string;
    phone1: string;
    phone2: string;
}
