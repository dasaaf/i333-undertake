export class Message {
    _id?: string;
    state: string;
    type: string;
    user_sender: string;
    user_receiver: string;
    content: string;
    created_date?: Date;
    updated_date?: Date;
    deleted_date?: Date;
}