import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  sendMessage(message: any) {
    let params = JSON.stringify(message);
    return this.http.post(Config.URL + '/contact', params, { headers: { 'Content-Type': 'application/json' } });
  }
}
