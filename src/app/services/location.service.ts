import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  token: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  getCountries() {
    return this.http.get<JSON>(Config.URL + '/location/countries', { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  getCountry(countryId: string) {
    return this.http.get<JSON>(Config.URL + '/location/country/' + countryId, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  getStates(countryId: string) {
    return this.http.get<JSON>(Config.URL + '/location/states/' + countryId, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  getState(stateId: string) {
    return this.http.get<JSON>(Config.URL + '/location/state/' + stateId, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  getCities(stateId: string) {
    return this.http.get<JSON>(Config.URL + '/location/cities/' + stateId, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  getCity(cityId: string) {
    return this.http.get<JSON>(Config.URL + '/location/city/' + cityId, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

}
