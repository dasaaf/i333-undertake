import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Negotiation } from '../models/negotiation';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class NegotiationService {
  token: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  createNegotiation(negotiation: Negotiation) {
    let params = JSON.stringify(negotiation);
    return this.http.post(Config.URL + '/negotiation', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
  }

  getNegotiations(interested_user: string, project?: string, page?: number, records_number?: number, sort?: string, sort_order?: string) {
    let path = Config.URL + '/negotiations';
    path += '/' + interested_user;
    if (project) {
      path += '/' + project;
    }
    if (page) {
      path += '/' + page;
    }
    if (records_number) {
      path += '/' + records_number;
    }
    if (sort) {
      path += '/' + sort;
    }
    if (sort_order) {
      path += '/' + sort_order;
    }
    return this.http.get<any>(path, { headers: { "Authorization": 'token ' + this.token + '' }, observe: 'response' });
  }

}
