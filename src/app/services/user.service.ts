import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Config } from '../config';

@Injectable()
export class UserService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    login(username: string, password: string) {
        return this.http.post<any>(Config.URL + '/authenticate', { email: username, password: password }, { observe: 'response' });
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
    }

    getAll() {
        return this.http.get<User[]>(Config.URL + '/users', { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getExperts(records_number?: number, page?: number, sort?: string, sort_order?: string) {
        let path = Config.URL + '/experts';
        if (records_number) {
            path += '/' + records_number;
        }
        if (page) {
            path += '/' + page;
        }
        if (sort) {
            path += '/' + sort;
        }
        if (sort_order) {
            path += '/' + sort_order;
        }
        return this.http.get<User[]>(path, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getById(id: string) {
        return this.http.get(Config.URL + '/user/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getPhoto(user_id: string) {
        return this.http.get(Config.URL + '/user/photo/' + user_id, { responseType: 'blob', headers: { "Authorization": 'token ' + this.token + '' } });
    }

    create(user: User) {
        let params = JSON.stringify(user);
        return this.http.post(Config.URL + '/user', params, { headers: { 'Content-Type': 'application/json' } });
    }

    passwordRecovery(data: any) {
        let params = JSON.stringify(data);
        return this.http.put(Config.URL + '/user/password-recovery', params, { headers: { 'Content-Type': 'application/json' } });
    }

    resetPassword(data: any) {
        let params = JSON.stringify(data);
        return this.http.put(Config.URL + '/user/reset-password', params, { headers: { 'Content-Type': 'application/json' } })
    }

    verifyRecaptcha(captcha: any) {
        let params = JSON.stringify(captcha);
        return this.http.post(Config.URL + '/verify-recaptcha', params, { headers: { 'Content-Type': 'application/json' } });
    }

    update(user: FormData, user_id: string, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('PUT', Config.URL + '/user/' + user_id, true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(user);
    }

    delete(id: string) {
        return this.http.delete(Config.URL + '/users/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    sendCurriculum(id: string) {
        let params = JSON.stringify({ name: "EXPERT", id: id });
        return this.http.post(Config.URL + '/role-request', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
    }

    removeCertificate(certificate: any) {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      const options = {
        headers: {'Authorization': 'token ' + this.token + '', 'Content-Type': 'application/json', 'user': user._id},
        body: {key: certificate.key}
      };
      return this.http.delete(Config.URL + '/remove-certificate/' + certificate._id, options);
    }

    downloadCertificate(certificate: any) {
      return this.http.get(Config.URL + '/download-certificate', { responseType: 'blob', headers: { Authorization: 'token ' + this.token + '', key: certificate.key}});
    }

}
