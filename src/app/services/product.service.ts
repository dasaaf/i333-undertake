import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';
import { Config } from '../config';

@Injectable()

export class ProductService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    getAll(project_id: string, records_number?: number, page?: number, sort?: string, sort_order?: string) {
        let path = Config.URL + '/products';
        path += '/' + project_id;

        if (records_number) {
            path += '/' + records_number;
        }
        if (page) {
            path += '/' + page;
        }
        if (sort) {
            path += '/' + sort;
        }
        if (sort_order) {
            path += '/' + sort_order;
        }
        return this.http.get<Product[]>(path, { observe: 'response' });
    }

    getById(id: string) {
        return this.http.get(Config.URL + '/product/' + id);
    }

    getImage(product_id: string, image_name: string) {
        return this.http.get(Config.URL + '/product/image/' + product_id + '/' + image_name, { responseType: 'blob' });
    }

    create(product: FormData, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('POST', Config.URL + '/product', true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(product);
    }

    update(product: FormData, _id: any, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('PUT', Config.URL + '/product/' + _id, true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(product);
    }

    publish(userId, productId) {
        return this.http.put(Config.URL + '/product/publish/' + userId + '/' + productId, null, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
    }

    delete(id: string) {
        return this.http.delete(Config.URL + '/product/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

}
