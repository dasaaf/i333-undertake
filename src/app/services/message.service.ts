import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '../models/message';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  token: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  createMessage(message: Message) {
    let params = JSON.stringify(message);
    return this.http.post(Config.URL + '/message', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
  }

  getMessages(userTransmitterId: string, userReceiverId: string) {
    return this.http.get<Message[]>(Config.URL + '/messages/' + userTransmitterId + '/' + userReceiverId, { headers: { "Authorization": 'token ' + this.token + '' }, observe: 'response' });
  }

  getConvertationsList(userId: string) {
    return this.http.get<Message[]>(Config.URL + '/convertations/' + userId, { headers: { "Authorization": 'token ' + this.token + '' }, observe: 'response' });
  }
  getMessage(message: string) {
    return this.http.get<any>(Config.URL + '/message/' + message, { headers: { "Authorization": 'token ' + this.token + '' }, observe: 'response' });
  }

  getFilePath(file_name: string) {
    return this.http.get(Config.URL + '/message/file/' + file_name, { headers: { "Authorization": 'token ' + this.token + '' }, responseType: 'arraybuffer' });
  }

  acceptProposal(message: Message) {
    let params = JSON.stringify(message);
    return this.http.put(Config.URL + '/message/accept/' + message._id, params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
  }
}
