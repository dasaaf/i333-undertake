import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config';

@Injectable()
export class MeService {

  constructor(private http: HttpClient) {

  }

  getMe(token: string) {
    return this.http.get<any>(Config.URL + '/me', { headers: { "Authorization": 'token ' + token + '' }, observe: 'response' });
  }
}
