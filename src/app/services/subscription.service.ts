import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from '../models/subscription';
import { Config } from '../config';

@Injectable()
export class SubscriptionService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    getSubscription(userId: string) {
        return this.http.get(Config.URL + '/subscription/' + userId, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    createSubscription(subscription: Subscription) {
        let params = JSON.stringify(subscription);
        return this.http.post(Config.URL + '/subscription', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
    }

}
