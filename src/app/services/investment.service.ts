import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Investment } from '../models/investment';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class InvestmentService {

  constructor(private http: HttpClient) { }

  getInvestments(investor_user: string) {
    return this.http.get<Investment[]>(Config.URL + '/investments/' + investor_user);
  }
  getInvestment(id:string){
    return this.http.get<Investment[]>(Config.URL + '/investment/' + id);
  }
  createInvestment(investment: Investment){
    let params = JSON.stringify(investment);
    let header = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(Config.URL + '/investment', params, {headers: header});
  }
}
