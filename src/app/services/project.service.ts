import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project';
import { Config } from '../config';

@Injectable()

export class ProjectService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    getAll(user_id?: string, records_number?: number, page?: number, sort?: string, sort_order?: string) {
        let path = Config.URL + '/projects';
        if (user_id) {
            path += '/' + user_id;
        }
        if (records_number) {
            path += '/' + records_number;
        }
        if (page) {
            path += '/' + page;
        }
        if (sort) {
            path += '/' + sort;
        }
        if (sort_order) {
            path += '/' + sort_order;
        }
        return this.http.get<Project[]>(path, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getCount(user_id?: string) {
        let path = Config.URL + '/count_projects';
        if (user_id) {
            path += '/' + user_id;
        }
        return this.http.get<Project[]>(path, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getById(id: string) {
        return this.http.get(Config.URL + '/project/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getImage(project_id: string) {
        return this.http.get(Config.URL + '/project/image/' + project_id, { responseType: 'blob', headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getFileAttached(file_name: string) {
        return this.http.get(Config.URL + '/project/file/' + file_name, { headers: { "Authorization": 'token ' + this.token + '' }, responseType: 'arraybuffer' });
    }

    create(project: FormData, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('POST', Config.URL + '/project', true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(project);
    }

    update(project: FormData, _id: any, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('PUT', Config.URL + '/project/' + _id, true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(project);
    }

    delete(id: string) {
        return this.http.delete(Config.URL + '/project/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }
}
