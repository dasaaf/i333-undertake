import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  notifications = this.socket.fromEvent<any>('chat');

  constructor(private socket: Socket) {
  }

  joinRoom(id: string) {
    this.socket.emit('joinroom', { user_id: id });
  }

  listenRoom() {
    this.socket.on('chat', (message) => {
      console.log(message);
    });
  }
}
