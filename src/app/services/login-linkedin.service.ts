import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config';

@Injectable()
export class LoginLinkedinService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    login() {
        const clientId = '78j8i7szy0lh26';
        const redirectUri = 'http://127.0.0.1:5001/auth/linkedin/callback';
        const state = 'initial';
        //const scope = 'r_liteprofile r_emailaddress w_member_social';
        const scope = 'r_liteprofile%20r_emailaddress%20w_member_social';
        const linkedinHost = "https://www.linkedin.com/oauth/v2/authorization";
        const parameters = `response_type=code&client_id=${clientId}&redirect_uri=${redirectUri}&state=${state}&scope=${scope}`;
        const payload = `${linkedinHost}?${parameters}`;
        return payload;
    }
}
