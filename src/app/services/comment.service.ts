import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/comment';
import { Config } from '../config';

@Injectable()

export class CommentService {
  token: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  getCommentList(expert_id) {
    let path = Config.URL + '/comments/' + expert_id;
    return this.http.get<Comment[]>(path, { headers: { "Authorization": 'token ' + this.token + '' } });
  }
  create(comment: Comment) {
    let params = JSON.stringify(comment);
    return this.http.post(Config.URL + '/comment', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
  }
}
