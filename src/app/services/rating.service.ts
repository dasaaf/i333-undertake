import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rating } from '../models/rating';
import { Config } from '../config';

@Injectable()

export class RatingService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    getRating(expertId: string, graderId: string) {
        return this.http.get(Config.URL + '/rating/' + expertId + '/' + graderId, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    getScore(expertId: string) {
        return this.http.get(Config.URL + '/rating/' + expertId, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    qualify(rating: Rating) {
        let params = JSON.stringify(rating);
        return this.http.post(Config.URL + '/rating', params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
    }

    updateQualify(rating: Rating) {
        let params = JSON.stringify(rating);
        return this.http.put(Config.URL + '/rating/' + rating._id, params, { headers: { "Authorization": 'token ' + this.token + '', 'Content-Type': 'application/json' } });
    }
}
