import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config';

@Injectable()

export class SearchService {
  token: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  searchProject(text: string, records_number, page) {
    return this.http.get<any>(Config.URL + '/search/project/' + text + '/' + records_number + '/' + page, { headers: { "Authorization": 'token ' + this.token + '' } });
  }

  searchExpert(text: string, records_number, page) {
    return this.http.get<any>(Config.URL + '/search/expert/' + text + '/' + records_number + '/' + page, { headers: { "Authorization": 'token ' + this.token + '' } });
  }
}
