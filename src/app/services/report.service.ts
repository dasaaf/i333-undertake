import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Report } from '../models/report';
import { Config } from '../config';

@Injectable()

export class ReportService {
    token: string;

    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }

    getAll(negotiation_id?: string, records_number?: number, page?: number, sort?: string, sort_order?: string) {
        let path = Config.URL + '/reports';
        if (negotiation_id) {
            path += '/' + negotiation_id;
        }
        if (records_number) {
            path += '/' + records_number;
        }
        if (page) {
            path += '/' + page;
        }
        if (sort) {
            path += '/' + sort;
        }
        if (sort_order) {
            path += '/' + sort_order;
        }
        return this.http.get<Report[]>(path, { headers: { "Authorization": 'token ' + this.token + '' }, observe: 'response' });
    }

    getById(id: string) {
        return this.http.get(Config.URL + '/report/' + id, { headers: { "Authorization": 'token ' + this.token + '' } });
    }

    create(report: FormData, callback) {
        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.response);
            }
        }
        xhr.open('POST', Config.URL + '/report', true);
        xhr.setRequestHeader("Authorization", 'token ' + this.token + '');
        xhr.send(report);
    }
}
